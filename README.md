# LearnThis

[![build status](https://gitlab.com/desarrolloutilyt/learnthis/badges/master/pipeline.svg)](https://gitlab.com/desarrolloutilyt/learnthis)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=desarrolloutilyt_learnthis&metric=ncloc)](https://sonarcloud.io/dashboard?id=desarrolloutilyt_learnthis)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=desarrolloutilyt_learnthis&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=desarrolloutilyt_learnthis)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=desarrolloutilyt_learnthis&metric=security_rating)](https://sonarcloud.io/dashboard?id=desarrolloutilyt_learnthis)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=desarrolloutilyt_learnthis&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=desarrolloutilyt_learnthis)


## Objetivo del proyecto 🎤

El objetivo del proyecto es realizar una plataforma web de aprendizaje que permita a cualquier persona publicar cursos de forma interactiva a través de vídeos y otros recursos. Se utilizarán tecnologías actuales enfocadas a la experiencia de usuario, rendimiento, usabilidad e interacción con la plataforma.

Este proyecto nace como una iniciativa en comunidad a través del canal de [Twitch](https://www.twitch.tv/desarrolloutil), de modo que cualquier persona interesada puede colaborar. El desarrollo del mismo se realizará retransmitiendo en directo.

## Stack software 📝

- [Next.JS](https://nextjs.org/): Framework frontend basado en [React](https://reactjs.org/) y orientado a SSR.
- [GraphQL](https://graphql.org/): Lenguaje de consultas para la API.
- [Nest.JS](https://nodejs.org/es/): Framework backend escalable y eficiente basado en [Express](https://expressjs.com/) y escrito en [TypeScript](https://www.typescriptlang.org/) que combina elementos de la POO, Programación Funcional y Programación Reactiva
- [MongoDB](https://www.mongodb.com/es): Sistema de base de datos NoSQL orientado a documentos
- [Jest](https://jestjs.io/): Marco de pruebas de JavaScript para el testeo del BackEnd

Puedes encontrar otras librerías y dependencias [aquí](/package.json).


## Herramientas 🛠️
- [Sonarcloud](https://sonarcloud.io/): Herramienta de **auditoría de código**, para detectar posibles errores y/o malas prácticas.
- [GraphQL Playground](https://www.apollographql.com/docs/apollo-server/testing/graphql-playground/): IDE interactivo para la gestión y pruebas de la API.
- [GitLab](https://gitlab.com/): Además de actuar como controlador de versiones, también se utiliza como CI/CD.


## Prerequisitos 📖

Antes de descargar el proyecto y sus dependencias, necesitas tener instalado NodeJS y su gestor de dependencias `npm`, que vienen en el mismo paquete de instalación. Puedes consultar [su página web oficial](https://nodejs.org/en/download/) para instalarlo, es multiplataforma (Windows, Linux e macOS).

También es conveniente que tengas instalado [Git](https://git-scm.com/downloads) para estar conectado al control de versiones si deseas contribuir al desarrollo del proyecto. Al igual que Node, es multiplataforma.

Por último, si quieres utilizar el proyecto con la configuración por defecto, necesitarás [Docker](https://www.docker.com/get-started), ya que es el encargado de montar la BBDD, SMTP de pruebas y Mongo-Express


## Lerna ⚙️

El repositorio y los proyectos están reunidos bajo [Lerna](https://github.com/lerna/lerna), una herramienta que nos permite mantener diferentes proyectos bajo un mismo repositorio (monorepo)

## Instalación ✔️

1.  Clonar el repositorio en nuestro workspace:

        git clone https://gitlab.com/desarrolloutilyt/learnthis/   

    *Es posible descargarlo directamente como un ZIP desde el botón CLONE.*

2.  Instalar Lerna:

    Sobre la carpeta raíz del proyecto, ejecutamos:

        npm i

3.  Inicializar proyectos:

    Mediante el siguiente comando, Lerna se encargará de instalar las dependencias de todos los proyectos, y enlazarlos entre sí:

        (Windows)
        npm run initialize
        (Linux, Mac)
        sudo npm run initialize
        
4. Ajustar las variables de entorno

    Para poder funcionar correctamente, es necesario crear los archivos de entorno `.env` correspondientes a cada proyecto.

   4.1. Backend

    Nos situamos en la carpeta del servidor

        cd packages/server

    Ejecutamos el siguiente comando, para crear el archivo .env a partir del ejemplo:
        
        (Windows)
        copy example.env .env

        (Linux)
        cp example.env .env

    4.2. Frontend

    Nos situamos en la carpeta del frontend

        cd packages/app

    Ejecutamos el siguiente comando, para crear el archivo .env a partir del ejemplo:

        (Windows)
        copy example.env .env
        (Linux)
        cp example.env .env
        
## Puesta en marcha 💡

Una vez esté instalado, tendremos que arrancar los dos proyectos (Back y Front):

1. Backend

Nos situamos en la carpeta del servidor

    cd packages/server

Ejecutamos el siguiente comando, para inicializar los contenedores de Docker:

    docker-compose up -d

Arrancamos el servidor en modo desarrollo

    npm run start:dev
2. Frontend

Nos situamos en la carpeta del frontend

    cd packages/app
    
Arrancamos el servidor en modo desarrollo

    npm run dev



## Arquitectura 📐

Los proyectos se encuentran distribuidos en la carpeta [/packages](/packages)

### [Área pública](/packages/app)

Apartado frontend que contiene todos los apartados de la web donde accederán los alumnos. 

Podéis consultar las dependencias de este proyecto [aquí](packages/app/package.json)

### [Backend](/packages/server)

Apartado del backend encargado de los procesos de negocio y la comunicación con BBDD

Podéis consultar las dependencias de este proyecto [aquí](packages/server/package.json)

### [Librerías comunes](/packages/utils)

Conjunto de funcionalidades compartidas entre los diferentes proyectos

Podéis consultar las dependencias de este proyecto [aquí](packages/utils/package.json)

## Quiero contribuir 🔨

Puedes unirte a la comunidad a través de [Discord](https://discord.gg/2dkRtPm), también puedes seguir el desarrollo del proyecto en [Twitch](https://www.twitch.tv/desarrolloutil)

¡Anímate!, te estamos esperando.

## Quiero donar 🙏

Aún no disponemos de sistema de donaciones. Cuando esté disponible, se publicará.
