import { Test, TestingModule } from '@nestjs/testing';
import { Body, INestApplication } from '@nestjs/common';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { model, connect, Model, disconnect } from 'mongoose';
import { gql } from 'graphql-request';
import * as request from 'supertest';

import { AppModule } from './../src/app.module';
import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { StudentTokenSchema } from '@Students/schemas/token.schema';
import { TokenDocument } from '@Students/interfaces/token-document.interface';
import { Gender } from '@Common/enums/gender.enum';
import { join } from 'path';

const replSet = new MongoMemoryReplSet({
	replSet: { storageEngine: 'wiredTiger' },
	instanceOpts: [
		{
			port: 27018,
		},
	],
});

let authToken;

describe('Student (e2e)', () => {
	let app: INestApplication;

	beforeAll(async () => {
		//Setting up Database
		await replSet.waitUntilRunning();
		const uri = await replSet.getUri();
		connect(uri, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		});
		process.env.DATABASE_URI = uri;

		//Starting app
		const moduleFixture: TestingModule = await Test.createTestingModule({
			imports: [AppModule],
		}).compile();

		app = moduleFixture.createNestApplication();
		await app.init();
	});

	it('Student - Register', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "Albert"
								surname: "hvdev"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_register).toBe(true);
			});
	});

	it('Student - Register - Bad email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail"
								name: "Albert"
								surname: "hvdev"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_EMAIL);
			});
	});

	it('Student - Register - Bad name', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "A"
								surname: "hvdev"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_NAME);
			});
	});

	it('Student - Register - Bad surname', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "Albert"
								surname: "5"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_SURNAME);
			});
	});

	it('Student - Register - Bad password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "Albert"
								surname: "Maroto"
								password: "12312"
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_PASSWORD);
			});
	});

	it('Student - Activate Account', async () => {
		const tokenModel: Model<TokenDocument> = model(
			'StudentToken',
			StudentTokenSchema
		);
		const { token } = await tokenModel.findOne().exec();

		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
				mutation {
					student_activate_account(token: "${token}")
				}`,
			})
			.expect(200)
			.expect(async ({ body }) => {
				expect(body.data.student_activate_account).toBe(true);
				expect(await tokenModel.findOne().exec()).toBeNull();
			});
	});

	it('Student - Activate Account - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_activate_account(token: "asdqwqweaz2d")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.TOKEN);
			});
	});

	it('Student - Create Forgot Password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_create_forgot_password_token(email: "yoquese@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_create_forgot_password_token).toBe(true);
			});
	});

	it('Student - Create Forgot Password - Bad email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_create_forgot_password_token(email: "asd123@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.EMAIL);
			});
	});

	it('Student - Valid Forgot Password', async () => {
		const tokenModel: Model<TokenDocument> = model(
			'StudentToken',
			StudentTokenSchema
		);
		const { token } = await tokenModel.findOne().exec();

		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
				query {
					student_valid_forgot_password_token(token: "${token}")
				}`,
			})
			.expect(200)
			.expect(async ({ body }) => {
				expect(body.data.student_valid_forgot_password_token).toBe(true);
			});
	});

	it('Student - Valid Forgot Password - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_valid_forgot_password_token(token: "dsag3t4wds43")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.TOKEN);
			});
	});

	it('Student - Change Forgot Password', async () => {
		const tokenModel: Model<TokenDocument> = model(
			'StudentToken',
			StudentTokenSchema
		);
		const { token } = await tokenModel.findOne().exec();

		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
				mutation {
					student_change_forgot_password(input:{token: "${token}", newPassword: "asd123ASD."})
				}`,
			})
			.expect(200)
			.expect(async ({ body }) => {
				expect(body.data.student_change_forgot_password).toBe(true);
				expect(await tokenModel.findOne().exec()).toBeNull();
			});
	});

	it('Student - Change Forgot Password - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_change_forgot_password(
							input: {
								token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
								newPassword: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.TOKEN);
			});
	});

	it('Student - Login', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_login(
							input: { email: "yoquese@gmail.com", password: "asd123ASD." }
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(typeof body.data.student_login).toBe('string');
				authToken = body.data.student_login;
			});
	});

	it('Student - Login - Bad email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_login(
							input: { email: "Ilya2@gmail.com", password: "asd123ASD." }
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toEqual(ERROR_MESSAGES.LOGIN);
			});
	});

	it('Student - Login - Bad password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_login(
							input: { email: "Ilya@gmail.com", password: "asd123ASD" }
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.LOGIN);
			});
	});

	it('Student - Profile', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					query {
						student_profile {
							email
							name
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(typeof body.data.student_profile).toBe('object');
				expect(body.data.student_profile.name).toBe('Albert');
			});
	});

	it('Student - Profile - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer 3fr5w25f4str43`)
			.send({
				query: gql`
					query {
						student_profile {
							email
							name
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.UNAUTHORIZED);
			});
	});

	it('Student - Change Password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_password(newPassword: "asd123ASD.")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_change_password).toBe(true);
			});
	});

	it('Student - Change Password - Bad format', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_password(newPassword: "asd")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_PASSWORD);
			});
	});

	it('Student - Change Password - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_change_password(newPassword: "asd123ASD.")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.UNAUTHORIZED);
			});
	});

	it('Student - Change Email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_email(newEmail: "yoquese@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_change_email).toBe(true);
			});
	});

	it('Student - Change Email - Bad format', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_email(newEmail: "asd")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_EMAIL);
			});
	});

	it('Student - Change Email - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_change_email(newEmail: "yoquese@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.UNAUTHORIZED);
			});
	});

	it('Student - Change Username', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_username(newUsername: "Albert87")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_change_username).toBe(true);
			});
	});

	it('Student - Change Username - Bar Username', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_username(newUsername: "Albert._Master")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_USERNAME);
			});
	});

	it('Student - Change Username - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_change_username(newUsername: "Albert87")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.UNAUTHORIZED);
			});
	});

	it('Student - Change Profile', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "07/15/1990"
								bio: "Te cuento mi vida en verso"
							}
						) {
							name
							gender
							email
							bio
							birthDate
							photo
							surname
							username
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(typeof body.data.student_modify_profile).toBe('object');
				expect(body.data.student_modify_profile.name).toBe('Reiva');
				expect(body.data.student_modify_profile.gender).toBe(
					Gender.MALE.toUpperCase()
				);
			});
	});

	it('Student - Change Profile - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw"
								gender: "Male"
								birthDate: "01/01/1990"
								bio: "Te cuento mi vida en verso"
							}
						) {
							name
							gender
							email
							bio
							birthDate
							photo
							surname
							username
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.UNAUTHORIZED);
			});
	});

	it('Student - Change Profile - Bad BirthDate', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "31/02/2020"
								bio: "Te cuento mi vida en verso"
							}
						) {
							name
							gender
							email
							bio
							birthDate
							photo
							surname
							username
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_BIRTHDATE);
			});
	});

	it('Student - Change Profile - Bad Name', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva_Crack"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "07/15/1990"
								bio: "Te cuento mi vida en verso"
							}
						) {
							name
							gender
							email
							bio
							birthDate
							photo
							surname
							username
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_NAME);
			});
	});

	it('Student - Change Profile - Bad Surname', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw7"
								gender: "${Gender.MALE}"
								birthDate: "07/15/1990"
								bio: "Te cuento mi vida en verso"
							}
						) {
							name
							gender
							email
							bio
							birthDate
							photo
							surname
							username
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_SURNAME);
			});
	});

	it('Student - Change Profile - Bad Biography', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "07/15/1990"
								bio: "Pues eso"
							}
						) {
							name
							gender
							email
							bio
							birthDate
							photo
							surname
							username
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(ERROR_MESSAGES.FORMAT_BIO);
			});
	});

	it('Student - Change Profile - Bad Name and BirthDate', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva_Crack"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "15/07/1990"
								bio: "Te cuento mi vida en verso"
							}
						) {
							name
							gender
							email
							bio
							birthDate
							photo
							surname
							username
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message.split('. ')).toContain(
					ERROR_MESSAGES.FORMAT_NAME
				);
				expect(body.errors[0].message.split('. ')).toContain(
					ERROR_MESSAGES.FORMAT_BIRTHDATE
				);
			});
	});

	it('Student - Upload photo', () => {
		return request(app.getHttpServer())
			.post('/student/upload')
			.set('Authorization', `Bearer ${authToken}`)
			.attach('photo', join(process.cwd(), './photo.jpg'))
			.expect(201)
			.expect(({ body }) => {
				expect(typeof body.url).toBe('string');
			});
	});

	it('Student - Upload photo- Bad token', () => {
		return request(app.getHttpServer())
			.post('/student/upload')
			.set('Authorization', `Bearer ahipuesno`)
			.attach('photo', join(process.cwd(), './photo.jpg'))
			.expect(401);
	});

	it('Student - Upload photo - Bad extension', () => {
		return request(app.getHttpServer())
			.post('/student/upload')
			.set('Authorization', `Bearer ${authToken}`)
			.attach('photo', join(process.cwd(), './example.env'))
			.expect(400);
	});

	afterAll(async () => {
		await app.close();
		await disconnect();
		await replSet.stop();
	});
});
