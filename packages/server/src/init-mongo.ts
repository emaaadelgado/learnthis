import { StudentSchema } from './students/schemas/students.schema';
import { model, connect } from 'mongoose';
import { config } from 'dotenv';

config();

(async () => {
	const connection = await connect(process.env.DATABASE_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	});

	const studentModel = model('Student', StudentSchema);

	const student = await studentModel
		.findOne({ email: 'testing2@test.com' })
		.exec();

	if (!student)
		await studentModel.create({
			email: 'testing2@test.com',
			name: 'bullchit',
			surname: 'Tanenbaum',
			password: '$2b$12$l9ElSZrSp02bPP.XlsuSQeatkx1AQ7SefCW5VGXFf/vsoIGhPvaPm',
			active: true,
		});

	await connection.disconnect();
})();
