/**
 * Message errors enum
 */
export enum ERROR_MESSAGES {
	//Services
	EMAIL = 'Email no encontrado',
	TOKEN = 'Token no encontrado',
	STUDENT = 'Estudiante no encontrado',
	LOGIN = 'Login incorrecto',
	NOT_ACTIVATED = 'Su cuenta no está activada, revise el correo',
	EMAIL_IN_USE = 'El email ya está en uso',
	UNAUTHORIZED = 'Operación no permitida',
	SERVER = 'Error en el servidor, inténtelo de nuevo más tarde',
	// Format
	FORMAT_USERNAME = 'Formato de username incorrecto',
	FORMAT_BIO = 'Formato de bio incorrecto',
	FORMAT_BIRTHDATE = 'Formato de fecha de nacimiento incorrecto',
	FORMAT_GENDER = 'Género inválido',
	FORMAT_NAME = 'Formato de nombre inválido',
	FORMAT_SURNAME = 'Formato de apellidos inválido',
	FORMAT_PHOTO = 'Formato de foto de perfil inválido',
	FORMAT_PASSWORD = 'Formato de contraseña inválido',
	FORMAT_EMAIL = 'Formato de email inválido',
	FORMAT_TOKEN = 'Token inválido',
	//Pipes

	//Utils
	MAIL_ERROR = 'Error al mandar un mail',
}
