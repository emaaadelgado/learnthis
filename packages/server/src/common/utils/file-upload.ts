import { ImageTypes } from '@Common/enums/image-types.enum';
import { BadRequestException } from '@nestjs/common';
import { Request } from 'express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';

/**
 * Filter upload image by file extension function
 *
 * @param  {Request} _req
 * @param  {any} file
 * @param  {(error:Error,acceptFile:boolean)=>void} cb
 */
export const fileFilter = (
	_req: Request,
	file: any,
	cb: (error: Error, acceptFile: boolean) => void
) => {
	const regExpImg = new RegExp(
		'\\.(' + Object.values(ImageTypes).join('|') + ')$'
	);
	if (!file.originalname.toLocaleLowerCase().match(regExpImg)) {
		return cb(new BadRequestException('Only image files are allowed!'), false); //TODO not hardcode message
	}
	cb(null, true);
};

/**
 * Multer Storage config
 */
export const storage = diskStorage({
	destination: join(__dirname, '../../../uploads'), //TODO env uploads folder¿?
	filename: (req, file, callback) => {
		const fileExtName = extname(file.originalname);
		callback(null, `${req.user._id}${fileExtName}`);
	},
});
