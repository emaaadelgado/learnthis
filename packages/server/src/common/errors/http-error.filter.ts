import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import {
	ArgumentsHost,
	Catch,
	ExceptionFilter,
	HttpException,
	HttpStatus,
	InternalServerErrorException,
	Logger,
} from '@nestjs/common';
import { GqlContextType } from '@nestjs/graphql';
import { Response } from 'express';
import { Error as MongoError } from 'mongoose';
import { TimeoutError } from 'rxjs';

/**
 * Class for catch all excaptions producced while respond incoming request
 */
@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
	/**
	 * Error delegate function
	 *
	 * @param  {HttpException|MongoError|TimeoutError|Error} exception
	 * @param  {ArgumentsHost} host
	 */
	catch(
		exception: HttpException | MongoError | TimeoutError | Error,
		host: ArgumentsHost
	) {
		const type = host.getType<GqlContextType>();

		if (
			type === 'graphql' &&
			(exception instanceof MongoError ||
				exception instanceof TimeoutError ||
				exception instanceof Error) &&
			!(exception instanceof HttpException)
		) {
			//TODO Logging
			//const gqlHost = GqlArgumentsHost.create(host);
			Logger.error(exception, exception.stack, 'Error Filter');
			throw new InternalServerErrorException(ERROR_MESSAGES.SERVER);
		} else if (type === 'http') {
			const ctx = host.switchToHttp();
			const response = ctx.getResponse<Response>();
			if (exception instanceof HttpException) {
				let statusCode = exception.getStatus();
				const r = <any>exception.getResponse();
				response.status(statusCode).json(r);
			} else {
				Logger.error(exception, exception.stack, 'Error Filter');
				response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
					message: ERROR_MESSAGES.SERVER,
					statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
				});
			}
		}
	}
}
