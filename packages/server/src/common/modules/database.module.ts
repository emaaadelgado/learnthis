import { ConfigService } from '@nestjs/config';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { Env } from '@Common/enums/env.enum';

/**
 * Mongoose configuration module with env
 */
export const DatabaseModule = MongooseModule.forRootAsync({
	useFactory: (configService: ConfigService): MongooseModuleOptions => ({
		uri: configService.get(Env.DB_URI),
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	}),
	inject: [ConfigService],
});
