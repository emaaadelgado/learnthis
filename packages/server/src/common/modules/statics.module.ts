import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

/**
 * ServerStatis module configuration
 */
export const StaticsModule = ServeStaticModule.forRoot({
	rootPath: join(__dirname, '../../../uploads'), //TODO env uploads folder¿?
	serveRoot: '/uploads', //TODO env uploads endpoit¿?
});
