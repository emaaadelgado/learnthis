import { Schemas } from '@Students/enums/schemas.enum';
import { Schema } from 'mongoose';
import { TokenTypes } from '../enums/token-types.enum';

/**
 * Token DB Schema
 */
export const StudentTokenSchema = new Schema({
	token: { type: String, required: true, unique: true, index: true },
	student: {
		type: Schema.Types.ObjectId,
		ref: Schemas.STUDENT,
		required: true,
	},
	type: {
		type: String,
		required: true,
		enum: Object.values(TokenTypes),
	},
	expiresAt: {
		type: Date,
		required: true,
		index: { expires: 1, sparse: true },
	},
});
