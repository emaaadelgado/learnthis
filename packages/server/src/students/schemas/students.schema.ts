import { Schema } from 'mongoose';
import { Gender } from '@Common/enums/gender.enum';

/**
 * Student DB Schema
 */
export const StudentSchema = new Schema({
	email: { type: String, required: true, unique: true },
	name: { type: String, required: true },
	surname: { type: String, required: true },
	password: { type: String, required: true },
	active: { type: Boolean, required: true, default: false },
	username: { type: String },
	photo: { type: String },
	bio: { type: String },
	gender: {
		type: String,
		enum: Object.values(Gender),
	},
	birthDate: { type: Date },
});
