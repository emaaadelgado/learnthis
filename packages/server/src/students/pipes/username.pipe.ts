import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';

/**
 * Username pipe validator
 */
@Injectable()
export class UsernamePipe implements PipeTransform {
	/**
	 * Username handler validator
	 *
	 * @param  {string} value
	 * @returns string
	 */
	transform(value: string): string {
		if (!FormValidation.usernameValidation(value))
			throw new BadRequestException(ERROR_MESSAGES.FORMAT_USERNAME);
		return value;
	}
}
