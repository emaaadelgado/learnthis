import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation, tokenValidation } from 'learnthis-utils';
import { RecoverPasswordDto } from '../dto/recover-password.dto';

/**
 * RecoverPasswordDto pipe validator
 */
@Injectable()
export class RecoverPasswordPipe implements PipeTransform {
	/**
	 * RecoverPasswordDto handler validator
	 *
	 * @param  {RecoverPasswordDto} value
	 * @returns RecoverPasswordDto
	 */
	transform(value: RecoverPasswordDto): RecoverPasswordDto {
		const { newPassword, token } = value;

		if (!tokenValidation(token))
			throw new BadRequestException(ERROR_MESSAGES.FORMAT_TOKEN);

		if (!FormValidation.passwordValidation(newPassword))
			throw new BadRequestException(ERROR_MESSAGES.FORMAT_PASSWORD);

		return value;
	}
}
