import { BadRequestException } from '@nestjs/common';
import { PasswordPipe } from './password.pipe';

describe('PasswordPipe', () => {
	let passwordPipe: PasswordPipe;

	beforeAll(() => (passwordPipe = new PasswordPipe()));

	it('Contraseña correcta', () => {
		const password = 'asd123ASD.';
		expect(passwordPipe.transform(password)).toBe(password);
	});

	it('Contraseña incorrecto - Prueba 1', () => {
		const password = 'asd12';
		expect(() => passwordPipe.transform(password)).toThrow(BadRequestException);
	});

	it('Contraseña incorrecto - Prueba 2', () => {
		const password = 'asd12ág';
		expect(() => passwordPipe.transform(password)).toThrow(BadRequestException);
	});
});
