import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { tokenValidation } from 'learnthis-utils';

/**
 * Token pipe validator
 */
@Injectable()
export class TokenPipe implements PipeTransform {
	/**
	 * Token handler validator
	 *
	 * @param  {string} value
	 * @returns string
	 */
	transform(value: string): string {
		if (!tokenValidation(value))
			throw new BadRequestException(ERROR_MESSAGES.TOKEN);

		return value;
	}
}
