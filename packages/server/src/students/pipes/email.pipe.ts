import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';

/**
 * Email pipe implementation
 */
@Injectable()
export class EmailPipe implements PipeTransform {
	/**
	 * Check if value is an email
	 *
	 * @param  {string} value
	 * @returns string
	 */
	transform(value: string): string {
		if (!FormValidation.emailValidation(value))
			throw new BadRequestException(ERROR_MESSAGES.FORMAT_EMAIL);
		return value;
	}
}
