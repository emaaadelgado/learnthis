import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { Gender } from '@Common/enums/gender.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';
import { ModifyProfileDto } from '../dto/modify-profile.dto';

/**
 * ModifyProfileDto pipe validator
 */
@Injectable()
export class ModifyProfilePipe implements PipeTransform {
	/**
	 * ModifyProfileDto handler validator
	 *
	 * @param  {ModifyProfileDto} value
	 * @returns ModifyProfileDto
	 */
	transform(value: ModifyProfileDto): ModifyProfileDto {
		const { bio, birthDate, gender, name, surname } = value;
		const errors = [];

		const allowedGenders: string[] = [
			Gender.MALE,
			Gender.FEMALE,
			Gender.OTHER_GENDER,
			Gender.RATHER_NOT_SAY,
		];

		if (bio && !FormValidation.bioValidation(bio))
			errors.push(ERROR_MESSAGES.FORMAT_BIO);

		if (birthDate && !FormValidation.birthDateValidation(birthDate))
			errors.push(ERROR_MESSAGES.FORMAT_BIRTHDATE);

		if (gender && !allowedGenders.includes(gender))
			errors.push(ERROR_MESSAGES.FORMAT_GENDER);

		if (name && !FormValidation.nameValidation(name))
			errors.push(ERROR_MESSAGES.FORMAT_NAME);

		if (surname && !FormValidation.nameValidation(surname))
			errors.push(ERROR_MESSAGES.FORMAT_SURNAME);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		return value;
	}
}
