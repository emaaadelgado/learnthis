import { BadRequestException } from '@nestjs/common';
import { UsernamePipe } from './username.pipe';

describe('UsernamePipe', () => {
	let usernamePipe: UsernamePipe;

	beforeAll(() => (usernamePipe = new UsernamePipe()));

	it('Username válido', () => {
		const username = 'Reiva_Master';
		expect(usernamePipe.transform(username)).toBe(username);
	});

	it('Username inválido', () => {
		const username = '_Reiva';
		expect(() => usernamePipe.transform(username)).toThrow(BadRequestException);
	});
});
