import { Gender } from '@Common/enums/gender.enum';
import { BadRequestException } from '@nestjs/common';
import { ModifyProfileDto } from '@Students/dto/modify-profile.dto';
import { ModifyProfilePipe } from './modify-profile.pipe';

describe('ModifyProfilePipe', () => {
	let modifyProfilePipe: ModifyProfilePipe;

	beforeAll(() => (modifyProfilePipe = new ModifyProfilePipe()));

	it('Todo correcto', () => {
		const profile: ModifyProfileDto = {
			bio: 'Te cuento mi vida y porque no se que más contar',
			birthDate: new Date('01/01/1990'),
			gender: Gender.MALE,
			name: 'Mariano',
			surname: 'Rajoy',
		};
		expect(modifyProfilePipe.transform(profile)).toBe(profile);
	});

	it('Fecha incorrecta', () => {
		const profile: ModifyProfileDto = {
			bio: 'Te cuento mi vida y porque no se que más contar',
			birthDate: new Date('03/15/2008'),
			gender: Gender.MALE,
			name: 'Mariano',
			surname: 'Rajoy',
		};
		expect(() => modifyProfilePipe.transform(profile)).toThrow(
			BadRequestException
		);
	});

	it('Género incorrecto', () => {
		const profile: ModifyProfileDto = {
			bio: 'Te cuento mi vida y porque no se que más contar',
			birthDate: new Date('01/01/1990'),
			gender: 'Calvo',
			name: 'Mariano',
			surname: 'Rajoy',
		};
		expect(() => modifyProfilePipe.transform(profile)).toThrow(
			BadRequestException
		);
	});

	it('Nombre incorrecto', () => {
		const profile: ModifyProfileDto = {
			bio: 'Te cuento mi vida y porque no se que más contar',
			birthDate: new Date('01/01/1990'),
			gender: Gender.MALE,
			name: '12',
			surname: 'Rajoy',
		};
		expect(() => modifyProfilePipe.transform(profile)).toThrow(
			BadRequestException
		);
	});

	it('Apellido incorrecto', () => {
		const profile: ModifyProfileDto = {
			bio: 'Te cuento mi vida y porque no se que más contar',
			birthDate: new Date('01/01/1990'),
			gender: Gender.MALE,
			name: 'Mariano',
			surname: 'Rajoy2',
		};
		expect(() => modifyProfilePipe.transform(profile)).toThrow(
			BadRequestException
		);
	});

	it('Biografía incorrecta', () => {
		const profile: ModifyProfileDto = {
			bio: 'Pues eso',
			birthDate: new Date('01/01/1990'),
			gender: Gender.MALE,
			name: 'Mariano',
			surname: 'Rajoy',
		};
		expect(() => modifyProfilePipe.transform(profile)).toThrow(
			BadRequestException
		);
	});
});
