import { FileProcessService } from '@Common/utils/file-process.service';
import { fileFilter, storage } from '@Common/utils/file-upload';
import {
	Controller,
	Post,
	UploadedFile,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { GetRestStudent } from '@Students/auth/get-student.decorator';
import { UploadAuthGuard } from '@Students/auth/jwt-upload.guard';
import { StudentDocument } from '@Students/interfaces/student-document.interface';
import { StudentsService } from '@Students/services/students.service';
import { join } from 'path';

/**
 * Student upload photo controller
 */
@Controller('student')
export class StudentController {
	constructor(
		private studentService: StudentsService,
		private fileProcessService: FileProcessService
	) {}

	/**
	 * Endpoint upload student photo
	 *
	 * @param  {} file
	 * @param  {StudentDocument} student
	 * @returns Object
	 */
	@Post('upload')
	@UseGuards(UploadAuthGuard)
	@UseInterceptors(
		FileInterceptor('photo', {
			fileFilter,
			storage,
		})
	)
	async upload(
		@UploadedFile() file,
		@GetRestStudent() student: StudentDocument
	): Promise<{ url: string }> {
		this.fileProcessService.transformImage(
			join(__dirname, '../../../uploads/', file.filename)
		); //TODO env uploads folder¿?
		return {
			url: await this.studentService.setPhoto(student, file.filename),
		};
	}
}
