/**
 * DB token types
 */
export enum TokenTypes {
	RECOVER_TOKEN = 'RecoverToken',
	ACTIVATE_TOKEN = 'ActivateToken',
}
