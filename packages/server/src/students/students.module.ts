import { Env } from '@Common/enums/env.enum';
import { MailService } from '@Common/utils/mail.service';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './auth/jwt-student.strategy';
import { Schemas } from './enums/schemas.enum';
import { StudentsResolver } from './resolvers/students.resolver';
import { StudentSchema } from './schemas/students.schema';
import { StudentTokenSchema } from './schemas/token.schema';
import { StudentsService } from './services/students.service';
import { TokenService } from './services/token.service';
import { StudentController } from './controllers/student.controller';
import { MulterModule } from '@nestjs/platform-express';
import { storage } from '@Common/utils/file-upload';
import { FileProcessService } from '@Common/utils/file-process.service';

/**
 * @ignore
 */
@Module({
	imports: [
		PassportModule.register({
			defaultStrategy: 'jwt',
		}),
		JwtModule.registerAsync({
			inject: [ConfigService],
			useFactory: (configService: ConfigService): JwtModuleOptions => ({
				secret: configService.get(Env.TOKEN_KEY),
			}),
		}),
		MongooseModule.forFeature([
			{ name: Schemas.STUDENT, schema: StudentSchema },
			{ name: Schemas.TOKEN, schema: StudentTokenSchema },
		]),
		MulterModule.register({
			storage,
			limits: {
				fieldSize: 5000000,
			},
		}),
	],
	providers: [
		TokenService,
		StudentsService,
		MailService,
		FileProcessService,
		StudentsResolver,
		JwtStrategy,
	],
	controllers: [StudentController],
})
export class StudentsModule {}
