import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { GetGqlStudent } from '@Students/auth/get-student.decorator';
import { GqlAuthGuard } from '@Students/auth/jwt-student.guard';
import { StudentDocument } from '@Students/interfaces/student-document.interface';
import { TokenService } from '@Students/services/token.service';
import { LoginDto } from '../dto/login.dto';
import { ModifyProfileDto } from '../dto/modify-profile.dto';
import { RecoverPasswordDto } from '../dto/recover-password.dto';
import { RegisterDto } from '../dto/register.dto';
import { EmailPipe } from '../pipes/email.pipe';
import { LoginPipe } from '../pipes/login.pipe';
import { ModifyProfilePipe } from '../pipes/modify-profile.pipe';
import { PasswordPipe } from '../pipes/password.pipe';
import { RecoverPasswordPipe } from '../pipes/recover-password.pipe';
import { RegisterPipe } from '../pipes/register.pipe';
import { TokenPipe } from '../pipes/token.pipe';
import { UsernamePipe } from '../pipes/username.pipe';
import { StudentsService } from '../services/students.service';
import { Student } from '../types/student.type';

@Resolver(of => Student)
export class StudentsResolver {
	constructor(
		private readonly studentService: StudentsService,
		private readonly tokenService: TokenService
	) {}

	@Query(returns => String)
	async student_login(
		@Args('input', { type: () => LoginDto }, LoginPipe)
		input: LoginDto
	): Promise<string> {
		return this.studentService.login(input);
	}

	@Query(returns => Boolean)
	async student_valid_forgot_password_token(
		@Args('token', TokenPipe) token: string
	): Promise<boolean> {
		await this.tokenService.checkToken(token);
		return true;
	}

	@Query(returns => Student)
	@UseGuards(GqlAuthGuard)
	async student_profile(
		@GetGqlStudent() student: StudentDocument
	): Promise<Student> {
		return student;
	}

	@Mutation(returns => Boolean)
	async student_register(
		@Args('input', { type: () => RegisterDto }, RegisterPipe)
		input: RegisterDto
	): Promise<boolean> {
		await this.studentService.register(input);
		return true;
	}

	@Mutation(returns => Boolean)
	async student_activate_account(
		@Args('token', TokenPipe) token: string
	): Promise<boolean> {
		return this.studentService.activateAccount(token);
	}

	@Mutation(returns => Boolean)
	async student_create_forgot_password_token(
		@Args('email', EmailPipe) email: string
	): Promise<boolean> {
		return this.studentService.createForgotPasswordToken(email);
	}

	@Mutation(returns => Boolean)
	async student_change_forgot_password(
		@Args('input', { type: () => RecoverPasswordDto }, RecoverPasswordPipe)
		input: RecoverPasswordDto
	): Promise<boolean> {
		return this.studentService.changeForgotPassword(input);
	}

	@Mutation(returns => Boolean)
	@UseGuards(GqlAuthGuard)
	async student_change_password(
		@GetGqlStudent() student: StudentDocument,
		@Args('newPassword', PasswordPipe) newPassword: string
	): Promise<boolean> {
		return this.studentService.changePassword(student, newPassword);
	}

	@Mutation(returns => Boolean)
	@UseGuards(GqlAuthGuard)
	async student_change_email(
		@GetGqlStudent() student: StudentDocument,
		@Args('newEmail', EmailPipe) newEmail: string
	): Promise<boolean> {
		return this.studentService.changeEmail(student, newEmail);
	}

	@Mutation(returns => Boolean)
	@UseGuards(GqlAuthGuard)
	async student_change_username(
		@GetGqlStudent() student: StudentDocument,
		@Args('newUsername', UsernamePipe) newUsername: string
	): Promise<boolean> {
		return this.studentService.changeUsername(student, newUsername);
	}

	@Mutation(returns => Student)
	@UseGuards(GqlAuthGuard)
	async student_modify_profile(
		@GetGqlStudent() student: StudentDocument,
		@Args('input', { type: () => ModifyProfileDto }, ModifyProfilePipe)
		input: ModifyProfileDto
	): Promise<Student> {
		return this.studentService.modifyProfile(student, input);
	}
}
