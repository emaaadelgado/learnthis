import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import {
	ExecutionContext,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';

/**
 * GraphQL Auth provider guard
 */
@Injectable()
export class GqlAuthGuard extends AuthGuard('jwt') {
	/**
	 * Function to generate correct context for passport
	 *
	 * @param  {ExecutionContext} context
	 */
	canActivate(context: ExecutionContext) {
		const ctx = GqlExecutionContext.create(context);
		const { req } = ctx.getContext();
		return super.canActivate(new ExecutionContextHost([req]));
	}

	/**
	 * Handler for auth request and return object identification user to passport
	 *
	 * @param  {any} err
	 * @param  {any} user
	 * @returns any
	 */
	handleRequest(err: any, user: any): any {
		if (err || !user) {
			throw err || new UnauthorizedException(ERROR_MESSAGES.UNAUTHORIZED);
		}
		return user;
	}
}
