import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { StudentDocument } from '@Students/interfaces/student-document.interface';

/**
 * GQL decorator for get auth user
 */
export const GetGqlStudent = createParamDecorator(
	(_, ctx: GqlExecutionContext): StudentDocument =>
		GqlExecutionContext.create(ctx).getContext().req.user
);

/**
 * Rest decorator for get auth user
 */
export const GetRestStudent = createParamDecorator(
	(data: unknown, ctx: ExecutionContext) => {
		const request = ctx.switchToHttp().getRequest();
		return request.user;
	}
);
