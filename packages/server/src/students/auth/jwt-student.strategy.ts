import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { Env } from '@Common/enums/env.enum';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { StudentTokenPayload } from '@Students/interfaces/user-payload.interface';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { StudentsService } from '../services/students.service';
import { StudentDocument } from '@Students/interfaces/student-document.interface';

/**
 * JWT custom implementation
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	/**
	 * @ignore
	 */
	constructor(
		private readonly studentsService: StudentsService,
		configService: ConfigService
	) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			secretOrKey: configService.get(Env.TOKEN_KEY),
		});
	}

	/**
	 * JWT Validation function
	 *
	 * @param  {StudentTokenPayload} payload
	 * @returns Promise
	 */
	async validate(payload: StudentTokenPayload): Promise<StudentDocument> {
		const { id } = payload;
		const student = await this.studentsService.findById(id);

		if (!student) throw new UnauthorizedException(ERROR_MESSAGES.UNAUTHORIZED);

		return student;
	}
}
