import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import {
	ExecutionContext,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

/**
 * Rest Auth guard
 */
@Injectable()
export class UploadAuthGuard extends AuthGuard('jwt') {
	/**
	 * @ignore
	 */
	canActivate(context: ExecutionContext) {
		return super.canActivate(context);
	}

	/**
	 * Handler for auth request and return object identification user to passport
	 *
	 * @param  {any} err
	 * @param  {any} user
	 * @returns any
	 */
	handleRequest(err: any, user: any): any {
		if (err || !user) {
			throw err || new UnauthorizedException(ERROR_MESSAGES.UNAUTHORIZED);
		}
		return user;
	}
}
