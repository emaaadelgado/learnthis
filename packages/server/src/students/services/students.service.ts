import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { Env } from '@Common/enums/env.enum';
import { MailService } from '@Common/utils/mail.service';
import {
	ConflictException,
	Injectable,
	NotFoundException,
	UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { TokenTypes } from '@Students/enums/token-types.enum';
import { StudentTokenPayload } from '@Students/interfaces/user-payload.interface';
import { compare, hash } from 'bcrypt';
import { ClientSession, Model } from 'mongoose';
import { LoginDto } from '../dto/login.dto';
import { ModifyProfileDto } from '../dto/modify-profile.dto';
import { RecoverPasswordDto } from '../dto/recover-password.dto';
import { RegisterDto } from '../dto/register.dto';
import { Schemas } from '../enums/schemas.enum';
import { StudentDocument } from '../interfaces/student-document.interface';
import { TokenService } from './token.service';

/**
 * Service for manage Student Model logic
 */
@Injectable()
export class StudentsService {
	/**
	 * @ignore
	 */
	constructor(
		@InjectModel(Schemas.STUDENT)
		private readonly studentModel: Model<StudentDocument>,
		private readonly configService: ConfigService,
		private readonly jwtService: JwtService,
		private readonly tokenService: TokenService,
		private readonly mailService: MailService
	) {}

	/**
	 * Model search by email
	 *
	 * @param  {string} email
	 * @returns Promise
	 */
	findByEmail(email: string): Promise<StudentDocument> {
		return this.studentModel
			.findOne({
				email: email.toLowerCase(),
			})
			.exec();
	}

	/**
	 * Model search by Identificator DB
	 *
	 * @param  {string} id
	 * @returns Promise
	 */
	findById(id: string): Promise<StudentDocument> {
		return this.studentModel.findById(id).exec();
	}

	/**
	 * Login jwt token genertor
	 *
	 * @param  {LoginDto}
	 * @returns Promise
	 */
	async login({ email, password }: LoginDto): Promise<string> {
		const student = await this.findByEmail(email);
		if (!student) throw new UnauthorizedException(ERROR_MESSAGES.LOGIN);

		const checkPass = await compare(password, student.password);
		if (!checkPass) throw new UnauthorizedException(ERROR_MESSAGES.LOGIN);

		if (!student.active)
			throw new UnauthorizedException(ERROR_MESSAGES.NOT_ACTIVATED);

		const payload: StudentTokenPayload = {
			id: student._id.toString(),
		};

		const token = this.jwtService.sign(payload, {
			expiresIn: this.configService.get(Env.TOKEN_EXPIRATION),
		});

		return token;
	}
	/**
	 * Register transaction
	 *
	 * @param  {ClientSession} session
	 * @param  {RegisterDto} register info
	 */
	private async createNewUserTransaction(
		session: ClientSession,
		{ email, name, surname, password }: RegisterDto
	) {
		const hashedPassword = await hash(password, 10);

		const student = (
			await this.studentModel.create(
				[
					{
						email: email.toLowerCase(),
						name: name,
						surname: surname,
						password: hashedPassword,
						active: false,
					},
				],
				{ session }
			)
		)[0];

		const token = await this.tokenService.generateToken(
			student._id.toString(),
			TokenTypes.ACTIVATE_TOKEN,
			session
		);

		await this.mailService.sendActivationMail(email, name, token);
	}

	/**
	 * Register new student
	 *
	 * @param  {RegisterDto}
	 * @returns Promise
	 */
	async register({
		email,
		name,
		surname,
		password,
	}: RegisterDto): Promise<void> {
		const existUser = await this.findByEmail(email);
		if (existUser) throw new ConflictException(ERROR_MESSAGES.EMAIL_IN_USE);

		const dbSession = await this.studentModel.db.startSession();
		await dbSession.withTransaction(sesaion =>
			this.createNewUserTransaction(sesaion, {
				email,
				name,
				surname,
				password,
			})
		);

		dbSession.endSession();
	}

	/**
	 * Generate token for forgot password
	 *
	 * @param  {string} email
	 * @returns Promise
	 */
	async createForgotPasswordToken(email: string): Promise<boolean> {
		const student = await this.findByEmail(email);
		if (!student) throw new NotFoundException(ERROR_MESSAGES.EMAIL);

		const token = await this.tokenService.generateToken(
			student._id.toString(),
			TokenTypes.RECOVER_TOKEN
		);

		await this.mailService.sendForgotPasswordMail(
			student.email,
			student.name,
			token
		);

		return true;
	}

	/**
	 * Student change forgot password and remove mandatory token
	 *
	 * @param  {RecoverPasswordDto}
	 * @returns Promise
	 */
	async changeForgotPassword({
		token,
		newPassword,
	}: RecoverPasswordDto): Promise<boolean> {
		const setNewPassword = async (session: ClientSession) => {
			const payload: StudentTokenPayload = await this.tokenService.checkToken(
				token
			);

			const student = await this.findById(payload.id);
			if (!student) throw new NotFoundException(ERROR_MESSAGES.EMAIL);

			student.password = await hash(newPassword, 10);

			await this.tokenService.removeToken(token, session);
			await student.save({ session });
		};

		const dbSession = await this.studentModel.db.startSession();
		await dbSession.withTransaction(() => setNewPassword(dbSession));

		dbSession.endSession();

		return true;
	}

	/**
	 * Student model activate
	 *
	 * @param  {string} token
	 * @returns Promise
	 */
	async activateAccount(token: string): Promise<boolean> {
		const activateNewAccount = async (session: ClientSession) => {
			const payload: StudentTokenPayload = await this.tokenService.checkToken(
				token
			);

			const student = await this.findById(payload.id);
			if (!student) throw new NotFoundException(ERROR_MESSAGES.EMAIL);

			student.active = true;

			await this.tokenService.removeToken(token, session);
			await student.save({ session });
		};

		const dbSession = await this.studentModel.db.startSession();
		await dbSession.withTransaction(() => activateNewAccount(dbSession));

		dbSession.endSession();

		return true;
	}

	/**
	 * Set student model password property with hash
	 *
	 * @param  {StudentDocument} student
	 * @param  {string} newPassword
	 * @returns Promise
	 */
	async changePassword(
		student: StudentDocument,
		newPassword: string
	): Promise<boolean> {
		student.password = await hash(newPassword, 10);
		await student.save();
		return true;
	}

	/**
	 * Set student model email property
	 *
	 * @param  {StudentDocument} student
	 * @param  {string} newEmail
	 * @returns Promise
	 */
	async changeEmail(
		student: StudentDocument,
		newEmail: string
	): Promise<boolean> {
		student.email = newEmail;
		await student.save();
		return true;
	}

	/**
	 * @param  {StudentDocument} student
	 * @param  {string} newUsername
	 * @returns Promise
	 */
	async changeUsername(
		student: StudentDocument,
		newUsername: string
	): Promise<boolean> {
		student.username = newUsername;
		await student.save();
		return true;
	}

	/**
	 * Modify Model student profile info properties
	 *
	 * @param  {StudentDocument} student
	 * @param  {ModifyProfileDto} modifyProfileDto
	 * @returns Promise
	 */
	async modifyProfile(
		student: StudentDocument,
		modifyProfileDto: ModifyProfileDto
	): Promise<StudentDocument> {
		student.set({ ...modifyProfileDto });
		return await student.save();
	}

	/**
	 * Set student model photo property
	 *
	 * @param  {StudentDocument} student
	 * @param  {string} filename
	 * @returns Promise
	 */
	async setPhoto(student: StudentDocument, filename: string): Promise<string> {
		student.photo = filename;
		await student.save();
		return filename;
	}
}
