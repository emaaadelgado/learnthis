import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { StudentTokenPayload } from '@Students/interfaces/user-payload.interface';
import { Schemas } from '@Students/enums/schemas.enum';
import { TokenDocument } from '@Students/interfaces/token-document.interface';
import { TokenTypes } from '@Students/enums/token-types.enum';
import { ClientSession, Model } from 'mongoose';
import { ConfigService } from '@nestjs/config';
import { ERROR_MESSAGES } from '@Common/enums/errors.enum';
import { Env } from '@Common/enums/env.enum';

/**
 * Token Model service manager
 */
@Injectable()
export class TokenService {
	/**
	 * @ignore
	 */
	constructor(
		@InjectModel(Schemas.TOKEN)
		private readonly tokenModel: Model<TokenDocument>,
		private readonly configService: ConfigService,
		private readonly jwtService: JwtService
	) {}

	/**
	 * Tokens expiration definition by type
	 */
	private readonly TOKEN_EXPIRATION = {
		[TokenTypes.ACTIVATE_TOKEN]:
			1000 *
			60 *
			60 *
			24 *
			Number(this.configService.get(Env.TOKEN_ACTIVATE_LIFE)),
		[TokenTypes.RECOVER_TOKEN]:
			1000 *
			60 *
			60 *
			24 *
			Number(this.configService.get(Env.TOKEN_RECOVER_LIFE)),
	};

	/**
	 * JWT token sign generator and save to db
	 *
	 * @param  {string} studentId
	 * @param  {TokenTypes} type
	 * @param  {ClientSession} session?
	 * @returns Promise
	 */
	async generateToken(
		studentId: string,
		type: TokenTypes,
		session?: ClientSession
	): Promise<string> {
		const expiration = this.TOKEN_EXPIRATION[type];

		const payload: StudentTokenPayload = {
			id: studentId,
		};

		const token = this.jwtService.sign(payload, { expiresIn: expiration });

		if (session)
			await this.tokenModel.create(
				[
					{
						token,
						type,
						student: studentId,
						expiresAt: new Date(Date.now() + expiration),
					},
				],
				{ session }
			);
		else
			await this.tokenModel.create({
				token,
				type,
				student: studentId,
				expiresAt: new Date(Date.now() + expiration),
			});

		return token;
	}

	/**
	 * JWT token validation and check with db
	 *
	 * @param  {string} token
	 * @returns Promise
	 */
	async checkToken(token: string): Promise<StudentTokenPayload> {
		const tokenFound = await this.tokenModel.findOne({ token }).exec();
		if (tokenFound) {
			const payload: StudentTokenPayload | null = await this.jwtService
				.verifyAsync(tokenFound.token)
				.catch(e => null);
			if (payload) return payload;
		}
		throw new BadRequestException(ERROR_MESSAGES.TOKEN);
	}

	/**
	 * Token db remove method
	 *
	 * @param  {string} token
	 * @param  {ClientSession} session
	 * @returns Promise
	 */
	async removeToken(token: string, session: ClientSession): Promise<boolean> {
		const result = await this.tokenModel
			.deleteOne({ token }, { session })
			.exec();
		if (result.n !== 1 || result.ok !== 1)
			throw new BadRequestException(ERROR_MESSAGES.TOKEN);
		return true;
	}
}
