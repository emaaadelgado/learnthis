import { Gender } from '@Common/enums/gender.enum';
import { Document } from 'mongoose';

export interface StudentDocument extends Document {
	email: string;
	name: string;
	surname: string;
	password: string;
	active: boolean;
	username?: string;
	photo?: string;
	bio?: string;
	gender?: Gender;
	birthDate?: Date;
}
