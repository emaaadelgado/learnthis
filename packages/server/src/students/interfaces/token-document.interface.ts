import { Document } from 'mongoose';
import { StudentDocument } from './student-document.interface';
import { TokenTypes } from '../enums/token-types.enum';

export interface TokenDocument extends Document {
	token: string;
	student: string | StudentDocument;
	type: TokenTypes;
	expiresAt: Date;
}
