import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class ModifyProfileDto {
	@Field({ nullable: true })
	name: string;

	@Field({ nullable: true })
	surname: string;

	@Field({ nullable: true })
	bio: string;

	@Field({ nullable: true })
	gender: string;

	@Field({ nullable: true })
	birthDate: Date;
}
