import { Gender } from '@Common/enums/gender.enum';
import { ObjectType } from '@nestjs/graphql';

/**
 * GraphQL Student Definition
 */
@ObjectType()
export class Student {
	email: string;
	name: string;
	surname: string;
	photo?: string;
	bio?: string;
	username?: string;
	gender?: Gender;
	birthDate?: Date;
}
