import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as figlet from 'figlet';

/**
 * Start application entrypoint
 */
async function bootstrap() {
	const app = await NestFactory.create(AppModule);

	app.enableCors();

	// if (process.env.NODE_ENV === 'dev') require('./init-mongo');

	const port = process.env.PORT ? process.env.PORT : 3000;

	await app.listen(port);
	Logger.log(`Application is running on: http://localhost:${port}`, 'Main');
	figlet('LearnThis.DU', function (err, data) {
		if (err) return;
		Logger.debug('\n' + data);
	});
}
bootstrap();
