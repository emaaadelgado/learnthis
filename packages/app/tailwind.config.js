module.exports = {
	purge: ['./src/**/*.tsx'],
	future: {
		removeDeprecatedGapUtilities: true,
		purgeLayersByDefault: true,
	},
	theme: {
		screens: {
			xs: { max: '480px' },
			sm: { min: '481px', max: '768px' },
			md: { min: '769px', max: '1024px' },
			lg: { min: '1025px' },
			xssm: { max: '768px' },
			smmd: { min: '481px', max: '1024px' },
			mdlg: { min: '769px' },
		},
		extend: {},
	},
	variants: {},
	plugins: require('tailwindcssdu'),
};
