import { Student } from './student.interface';

export interface IAuthContext {
	student?: Student;
	jwt?: string;
}
