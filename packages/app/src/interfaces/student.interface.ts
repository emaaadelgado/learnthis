import { Gender } from './gender.enum';

export interface Student {
	email: string;
	name: string;
	surname: string;
	active: boolean;
	username?: string;
	photo?: string;
	bio?: string;
	gender?: Gender;
	birthDate?: Date;
}
