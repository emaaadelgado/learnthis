export interface IconProps {
	className?: string;
	[prop: string]: any;
}
