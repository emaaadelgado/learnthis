import { NormalizedCacheObject } from '@apollo/client';
import { Student } from '@Interfaces/student.interface';

export interface AuthProps {
	initialApolloState: NormalizedCacheObject;
	student: Student;
	jwt: string;
}

export interface GSSProps {
	authProps?: AuthProps;
	componentProps?: any;
}
