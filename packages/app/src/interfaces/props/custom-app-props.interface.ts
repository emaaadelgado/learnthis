import { ComponentType } from 'react';
import { GSSProps } from './gss-props.interface';

export interface CustomAppProps {
	Component: ComponentType;
	pageProps: GSSProps;
}
