import { RedirectConditions } from '@Lib/utils/ssr.utils';

export interface IRedirect {
	href: string;
	asPath?: string;
	statusCode: number;
	condition: RedirectConditions;
}
