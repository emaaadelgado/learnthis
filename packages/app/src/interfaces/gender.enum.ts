export enum Gender {
	MALE = 'Male',
	FEMALE = 'Female',
	OTHER_GENDER = 'Other gender',
	RATHER_NOT_SAY = 'Rather not say',
}
