export enum RestEndPoints {
	LOGIN = '/login',
	LOGOUT = '/logout',
}
