import {
	ApolloClient,
	HttpLink,
	InMemoryCache,
	NormalizedCacheObject,
} from '@apollo/client';
//import { onError } from '@apollo/client/link/error';
import { setContext } from '@apollo/client/link/context';

let apolloClient: ApolloClient<NormalizedCacheObject>;

export const createApolloClient = (jwt?: string) => {
	const authLink = setContext((_, { headers }) => {
		return {
			headers: {
				...headers,
				authorization: jwt ? `Bearer ${jwt}` : '',
			},
		};
	});

	// const errorLink = onError(({ graphQLErrors, networkError }) => {
	// 	if (graphQLErrors)
	// 		graphQLErrors.map(({ message, locations, path }) =>
	// 			console.log(
	// 				`[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
	// 					locations
	// 				)}, Path: ${path}`
	// 			)
	// 		);
	// 	if (networkError) console.log(`[Network error]: ${networkError}`);
	// });

	return new ApolloClient<NormalizedCacheObject>({
		ssrMode: typeof window === 'undefined',
		// link: errorLink.concat(
		// 	authLink.concat(
		// 		new HttpLink({
		// 			uri: process.env.NEXT_PUBLIC_GQL_URI,
		// 		})
		// 	)
		// ),
		link: authLink.concat(
			new HttpLink({
				uri: process.env.NEXT_PUBLIC_GQL_URI,
			})
		),
		cache: new InMemoryCache(),
	});
};

export function initializeApollo(
	jwt?: string,
	initialState?: NormalizedCacheObject
) {
	let _apolloClient: ApolloClient<NormalizedCacheObject> = apolloClient;

	if (!apolloClient || jwt) {
		_apolloClient = createApolloClient(jwt);
	}

	if (initialState) {
		const existingCache = _apolloClient.extract();
		_apolloClient.cache.restore({
			...existingCache,
			...initialState,
		} as any);
	}

	if (typeof window === 'undefined') return _apolloClient;

	if (!apolloClient) apolloClient = _apolloClient;

	return _apolloClient;
}
