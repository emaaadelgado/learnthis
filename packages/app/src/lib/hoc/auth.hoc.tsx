import { FC, useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { IRedirect } from '@Interfaces/redirect.interface';

import { AuthContext } from '@Lib/context/auth.context';
import { RedirectConditions } from '@Lib/utils/ssr.utils';

export const withAuth = (Component: FC, redirect: IRedirect) => {
	const { href, asPath, condition } = redirect;
	return (props: any) => {
		const router = useRouter();
		const { stateAuth } = useContext(AuthContext);
		const [shouldRender, setShouldRender] = useState<boolean>(false);

		useEffect(() => {
			if (
				(stateAuth.jwt &&
					condition === RedirectConditions.REDIRECT_WHEN_USER_EXISTS) ||
				(!stateAuth.jwt &&
					condition === RedirectConditions.REDIRECT_WHEN_USER_NOT_EXISTS)
			)
				router.replace(href, asPath);
			setShouldRender(true);
		}, []);

		return shouldRender ? <Component {...props}></Component> : <></>;
	};
};
