export const fetchWithTimeout = (route: string, req: any, timeout: number) => {
	const controller = new AbortController();
	setTimeout(() => controller.abort(), timeout);

	return fetch(`${process.env.NEXT_PUBLIC_API_URI}${route}`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(req),
		signal: controller.signal,
	});
};
