import { initializeApollo } from '@Lib/apollo-client';
import { GraphlStudent } from 'learnthis-utils';

import Cookie from 'cookie';
import { JWT_COOKIE_NAME } from '@Config/constants';
import { ServerResponse } from 'http';
import { Env } from '@Interfaces/env.enum';
import { AuthProps } from '@Interfaces/props/gss-props.interface';

export const getJwtFromCookie = (cookies?: string): string | void => {
	if (cookies) {
		return Cookie.parse(cookies)[JWT_COOKIE_NAME];
	}
};

export const removeJwtCookie = (res: ServerResponse): void => {
	const setCookie = Cookie.serialize(JWT_COOKIE_NAME, '', {
		httpOnly: true,
		path: '/',
		secure: process.env[Env.SECURE_COOKIE] !== 'false',
		sameSite: true,
		maxAge: 0,
	});
	res.setHeader('Set-Cookie', setCookie);
};

export const loadCurrentUserSSR = async (
	jwt: string
): Promise<AuthProps | undefined> => {
	const apolloClient = initializeApollo(jwt);

	const response = await apolloClient.query({
		query: GraphlStudent.student_profile_min,
	});

	const student = response.data.student_profile;

	if (!student) return;
	const authProps: AuthProps = {
		initialApolloState: apolloClient.cache.extract(),
		student,
		jwt,
	};

	return authProps;
};

export const checkActivationToken = async (token: string | string[]) => {
	const apolloClient = initializeApollo();

	try {
		if (typeof token === 'string') {
			const response = await apolloClient.mutate({
				mutation: GraphlStudent.student_activate_account,
				variables: { token },
			});
			if (response?.data?.student_activate_account) return true;
		}
		return false;
	} catch (e) {
		return false;
	}
};

export const checkRecoverToken = async (token: string | string[]) => {
	const apolloClient = initializeApollo();

	try {
		if (typeof token === 'string') {
			const response = await apolloClient.mutate({
				mutation: GraphlStudent.student_valid_forgot_password_token,
				variables: { token },
			});
			if (response?.data?.student_valid_forgot_password_token) return true;
		}
		return false;
	} catch (e) {
		return false;
	}
};
