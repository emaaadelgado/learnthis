import { AuthProps } from '@Interfaces/props/gss-props.interface';
import { IRedirect } from '@Interfaces/redirect.interface';
import { ServerResponse } from 'http';
import { GetServerSidePropsContext } from 'next';
import {
	getJwtFromCookie,
	loadCurrentUserSSR,
	removeJwtCookie,
} from './user.utils';

export enum RedirectConditions {
	REDIRECT_WHEN_USER_EXISTS = 0,
	REDIRECT_WHEN_USER_NOT_EXISTS = 1,
	REDIRECT_WHEN_TOKEN_NOT_EXISTS = 2,
}

export const isRequestSSR = (refererUrl?: string) => {
	if (!refererUrl) return true;

	const { host } = new URL(refererUrl);

	return process.env.HOST !== host;
};

export const serverRedirect = (
	res: ServerResponse,
	{ href, statusCode }: IRedirect
) => {
	res.setHeader('Location', href);
	res.statusCode = statusCode;
	res.end();
};

export const withAuthGSSP = async (
	{ req, res }: GetServerSidePropsContext,
	redirect?: IRedirect
): Promise<AuthProps | undefined> => {
	const isSSR = isRequestSSR(req.headers.referer);
	if (!isSSR) return;

	const { cookie } = req.headers;
	const jwt = getJwtFromCookie(cookie);

	if (!jwt) {
		if (
			redirect?.condition === RedirectConditions.REDIRECT_WHEN_USER_NOT_EXISTS
		) {
			serverRedirect(res, redirect);
		}
		return;
	} else {
		try {
			const authProps = await loadCurrentUserSSR(jwt);
			if (
				redirect?.condition === RedirectConditions.REDIRECT_WHEN_USER_EXISTS
			) {
				serverRedirect(res, redirect);
			}
			return authProps;
		} catch (error) {
			removeJwtCookie(res);
			if (
				redirect?.condition === RedirectConditions.REDIRECT_WHEN_USER_NOT_EXISTS
			) {
				serverRedirect(res, redirect);
			}
			return;
		}
	}
};
