export const cleanFirstLevelJson = function cleanJson(obj: any) {
	for (const key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key)) {
			const value = obj[key];
			if (value === undefined || value === null) delete obj[key];
		}
	}
	return obj;
};
