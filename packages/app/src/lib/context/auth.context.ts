import { IAuthContext } from '@Interfaces/auth-context.interface';
import { createContext, Dispatch } from 'react';

interface IAthContext {
	stateAuth: IAuthContext;
	setAuth: Dispatch<IAuthContext>;
}

export const AuthContext = createContext<IAthContext>({} as any);
