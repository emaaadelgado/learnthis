import LogoIcon from '@Components/icons/logo.icon';
import { FC } from 'react';

export type LoginLayoutProps = {
	title: string;
};

const LoginLayout: FC<LoginLayoutProps> = ({ title, children }) => (
	<div className='container-sm my-auto px-1'>
		<div className='rounded-1_5 shadow-xl p-1_5 mt-4 bg-white'>
			<div className='flex-c-s'>
				<div className='xssm:hidden mdlg:w-1/4 flex-c-c'>
					<LogoIcon className='h-6 mt-4' />
				</div>
				<div className='xssm:w-full mdlg:w-3/4 flex-c-c'>
					<div className='w-full max-w-20 py-1_5 xssm:text-center'>
						<h1 className='ml-1 text-teal-600 text-2xl font-semibold mb-1'>
							{title}
						</h1>
						{children}
					</div>
				</div>
			</div>
		</div>
	</div>
);

export default LoginLayout;
