import { FC, HTMLProps } from 'react';
import { InputTypes } from '@Interfaces/generic/input-types.enum';

export interface InputProps extends HTMLProps<HTMLInputElement> {
	label: string;
	type: InputTypes;
	error?: boolean;
	errorMsg?: string;
}

const Input: FC<InputProps> = ({
	label,
	type,
	className,
	error,
	errorMsg,
	...props
}) => {
	const classNames = ['flexcol-c-s'];
	const inputClassNames = ['w-full', 'px-1', 'py-0_5', 'rounded-lg', 'border'];

	className && classNames.push(className);
	className = classNames.join(' ');

	inputClassNames.push(
		error
			? 'border-red-600'
			: 'border-teal-500 focus:border-teal-700 focus:border-2'
	);

	const inputClassName = inputClassNames.join(' ');

	return (
		<div className={className}>
			<label className='mb-0_25 ml-0_5'>{label}</label>
			<input
				className={inputClassName}
				type={type}
				placeholder={label}
				{...props}></input>
			{error && (
				<p className='text-red-600 text-sm ml-1'>
					{errorMsg || `Formato de ${label} incorrecto`}
				</p>
			)}
		</div>
	);
};

export default Input;
