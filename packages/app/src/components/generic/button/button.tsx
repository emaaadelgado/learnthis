import { ComponentProps, FC } from 'react';
import { ButtonType, ButtonTypes } from '@Interfaces/generic/button-types.enum';

export interface ButtonProps extends ComponentProps<'button'> {
	label: string;
	kind: ButtonType;
}

const Button: FC<ButtonProps> = ({ label, kind, className, ...props }) => {
	const classNames = ['px-1', 'py-0_5', 'rounded-md'];

	const styles = {
		[ButtonTypes.PRIMARY]: ['text-white', 'bg-teal-600', 'hover:bg-teal-700'],
		[ButtonTypes.CTA]: ['text-white', 'bg-yellow-500', 'hover:bg-yellow-600'],
	};

	classNames.push(...styles[kind]);

	className && classNames.push(className);

	className = classNames.join(' ');

	return (
		<button className={className} {...props}>
			{label}
		</button>
	);
};

export default Button;
