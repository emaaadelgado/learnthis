import { FC } from 'react';
import Link, { LinkProps } from 'next/link';
import { ButtonType, ButtonTypes } from '@Interfaces/generic/button-types.enum';

export interface LinkButtonProps extends LinkProps {
	label: string;
	className: string;
	kind: ButtonType;
}

const LinkButton: FC<LinkButtonProps> = ({
	label,
	kind,
	className,
	...props
}) => {
	const classNames = ['px-1', 'py-0_5', 'rounded-md'];

	const styles = {
		[ButtonTypes.PRIMARY]: ['text-white', 'bg-teal-600', 'hover:bg-teal-700'],
		[ButtonTypes.CTA]: ['text-white', 'bg-yellow-500', 'hover:bg-yellow-600'],
	};

	classNames.push(...styles[kind]);

	className && classNames.push(className);

	className = classNames.join(' ');

	return (
		<Link {...props}>
			<a className={className}>{label}</a>
		</Link>
	);
};

export default LinkButton;
