import { FC, useState, MouseEvent, ComponentProps } from 'react';
import { IconProps } from '@Interfaces/props/icon-props.interface';

export interface IconSwapButtonProps extends ComponentProps<'button'> {
	Icon: FC<IconProps>;
	IconClicked: FC<IconProps>;
	iconClassname?: string;
	onClick: (event: MouseEvent<HTMLButtonElement> | undefined) => void;
}

const IconSwapButton: FC<IconSwapButtonProps> = ({
	Icon,
	IconClicked,
	className,
	iconClassname,
	onClick,
	...props
}) => {
	const [clicked, setClicked] = useState(false);

	const onClickWithState = (event: MouseEvent<HTMLButtonElement>): void => {
		setClicked(!clicked);
		onClick(event);
	};

	return (
		<button className={className} onClick={onClickWithState} {...props}>
			{!clicked ? (
				<Icon className={iconClassname} />
			) : (
				<IconClicked className={iconClassname} />
			)}
		</button>
	);
};

export default IconSwapButton;
