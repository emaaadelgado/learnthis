import { FC } from 'react';
import { IAuthContext } from '@Interfaces/auth-context.interface';
import Link from 'next/link';

import IconSwapButton from '@Components/generic/button/icon-swap-button';
import LinkButton from '@Components/generic/button/link-button';
import CartIcon from '@Components/icons/cart.icon';
import CloseIcon from '@Components/icons/close.icon';
import LogoTextIcon from '@Components/icons/logo-text.icon';
import MenuIcon from '@Components/icons/menu.icon';

export type HeaderProps = {
	stateAuth: IAuthContext;
};

const Header: FC<HeaderProps> = ({ stateAuth }) => (
	<header className=' bg-white'>
		<div className='container-xl flex-c-c px-1 py-0_75'>
			<div className='flex-s-c flex-1'>
				<Link href='/'>
					<a>
						<LogoTextIcon className='h-2_75' />
					</a>
				</Link>
				<div className='xssm:hidden flex-c-c px-2'>
					<Link href='/contacto'>
						<a className='px-0_5 py-0_25 font-semibold'>Cursos</a>
					</Link>
					<Link href='/contacto'>
						<a className='px-0_5 py-0_25 font-semibold'>Sobre nosotros</a>
					</Link>
					<Link href='/contacto'>
						<a className='px-0_5 py-0_25 font-semibold'>Contacto</a>
					</Link>
				</div>
			</div>
			<div className='flex-e-c flex-1'>
				<CartIcon className='h-1_75 mr-1_5' />
				{stateAuth.student ? (
					<b>{stateAuth.student.name}</b>
				) : (
					<LinkButton
						className='xssm:hidden'
						href='/login'
						kind='primary'
						label='Inicia sesión'
					/>
				)}
				<IconSwapButton
					Icon={MenuIcon}
					IconClicked={CloseIcon}
					className='mdlg:hidden p-0_25'
					iconClassname='h-1_75'
					onClick={() => {}}
				/>
			</div>
		</div>
	</header>
);

export default Header;
