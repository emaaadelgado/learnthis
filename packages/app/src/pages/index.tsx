import { FC } from 'react';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';

import Head from '@Components/generic/head';

import { GSSProps } from '@Interfaces/props/gss-props.interface';

import { withAuthGSSP } from '@Lib/utils/ssr.utils';

const Home: FC = () => (
	<>
		<Head title='LearnThis' description='Una nueva forma de aprender' url='/' />
	</>
);

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = {};

	const authProps = await withAuthGSSP(ctx);
	if (authProps) props.authProps = authProps;

	return {
		props,
	};
};

export default Home;
