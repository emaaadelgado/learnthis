import { FC } from 'react';

import Head from '@Components/generic/head';

const NotFound: FC = () => (
	<>
		<Head title='Página no encontrada | Learnthis' noindex />
		<div className='container-sm my-auto px-1'>
			<div className='rounded-1_5 shadow-xl p-1_5 mt-4 bg-white text-center'>
				<h1 className='text-5xl font-semibold text-teal-600'>404</h1>
				<p>OOPS... parece que no hay nada aquí</p>
			</div>
		</div>
	</>
);

export default NotFound;
