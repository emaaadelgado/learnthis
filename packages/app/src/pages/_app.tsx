import { FC, useEffect, useState } from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import { ToastContainer } from 'react-toastify';

import Header from '@Components/header';

import { CustomAppProps } from '@Interfaces/props/custom-app-props.interface';
import { IAuthContext } from '@Interfaces/auth-context.interface';

import { initializeApollo } from '@Lib/apollo-client';
import { AuthContext } from '@Lib/context/auth.context';
import { toastSettings } from '@Config/toast.settings';

import 'react-toastify/dist/ReactToastify.min.css';
import '@Styles/index.css';

const App: FC<CustomAppProps> = ({ Component, pageProps }) => {
	const { authProps, componentProps } = pageProps;

	const [apolloClient, setApolloClient] = useState(
		initializeApollo(authProps?.jwt, authProps?.initialApolloState)
	);

	const [stateAuth, setAuth] = useState<IAuthContext>({
		student: authProps?.student,
		jwt: authProps?.jwt,
	});

	useEffect(() => {
		const newApolloClient = initializeApollo(
			stateAuth.jwt,
			apolloClient.cache.extract()
		);
		setApolloClient(newApolloClient);
	}, [stateAuth]);

	return (
		<ApolloProvider client={apolloClient}>
			<Header stateAuth={stateAuth} />
			<AuthContext.Provider value={{ stateAuth, setAuth }}>
				<Component {...componentProps} />
			</AuthContext.Provider>
			<ToastContainer {...toastSettings} />
		</ApolloProvider>
	);
};

export default App;
