import { JWT_COOKIE_NAME } from '@Config/constants';
import { HTTPStatusCodes } from '@Interfaces/http-status-codes.enum';
import cookie from 'cookie';
import { NextApiRequest, NextApiResponse } from 'next';

const logout = (req: NextApiRequest, res: NextApiResponse<boolean>) => {
	if (req.method !== 'POST') {
		res.status(HTTPStatusCodes.METHOD_NOT_ALLOWED).send(false);
		return;
	}
	const setCookie = cookie.serialize(JWT_COOKIE_NAME, '', {
		httpOnly: true,
		path: '/',
		// secure: true,
		sameSite: true,
		maxAge: 0,
	});
	res.setHeader('Set-Cookie', setCookie);
	res.status(HTTPStatusCodes.OK).send(true);
};

export default logout;
