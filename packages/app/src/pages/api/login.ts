import { GraphlStudent } from 'learnthis-utils';

import {
	ApolloClient,
	ApolloError,
	ApolloLink,
	HttpLink,
	InMemoryCache,
} from '@apollo/client';

import cookie from 'cookie';
import { Env } from '@Interfaces/env.enum';
import { NextApiRequest, NextApiResponse } from 'next';
import { JWT_COOKIE_NAME } from '@Config/constants';
import { HTTPStatusCodes } from '@Interfaces/http-status-codes.enum';

const login = async (req: NextApiRequest, res: NextApiResponse) => {
	if (req.method !== 'POST') {
		res.status(HTTPStatusCodes.METHOD_NOT_ALLOWED).send(false);
		return;
	}

	const { email, password } = req.body;

	const httpLink = new HttpLink({
		uri: process.env.NEXT_PUBLIC_GQL_URI,
		fetch,
	});

	const client = new ApolloClient({
		cache: new InMemoryCache(),
		link: ApolloLink.from([httpLink]),
		defaultOptions: {
			query: {
				fetchPolicy: 'network-only',
			},
		},
	});

	try {
		let response = await client.query({
			query: GraphlStudent.student_login,
			variables: {
				input: {
					email,
					password,
				},
			},
		});

		const token = response.data.student_login;

		if (token) {
			//TODO Resolve student data in login gql resolver
			response = await client.query({
				query: GraphlStudent.student_profile,
				context: { headers: { Authorization: `Bearer ${token}` } },
			});

			const user = response.data.student_profile;

			const setCookie = cookie.serialize(JWT_COOKIE_NAME, token, {
				httpOnly: true,
				path: '/',
				secure: process.env[Env.SECURE_COOKIE] !== 'false',
				sameSite: true,
				maxAge: Number(process.env[Env.COOKIE_EXPIRATION]),
			});
			res.setHeader('Set-Cookie', setCookie);
			res.status(HTTPStatusCodes.OK).send({ token, user });
		}
	} catch (error: any) {
		if (error instanceof ApolloError && error?.graphQLErrors[0]) {
			res
				.status(error.graphQLErrors[0].extensions?.exception?.status)
				.send({ error: error.message });
		} else {
			res
				.status(HTTPStatusCodes.INTERNAL_SERVER_ERROR)
				.send({ error: 'Error interno del servidor' });
		}
	}
};

export default login;
