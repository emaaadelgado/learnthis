import { FC, useEffect } from 'react';
import { GetServerSideProps } from 'next';

import { useRouter } from 'next/router';
import { toast } from 'react-toastify';

import { IRedirect } from '@Interfaces/redirect.interface';
import { HTTPStatusCodes } from '@Interfaces/http-status-codes.enum';
import { RedirectConditions, serverRedirect } from '@Lib/utils/ssr.utils';
import { AlertMessages } from '@Config/constants';
import { checkActivationToken, removeJwtCookie } from '@Lib/utils/user.utils';

const activateToken: FC = () => {
	const router = useRouter();

	useEffect(() => {
		router.push('/login');
		toast.success(AlertMessages.ACTIVATION_SUCCESS);
	}, [router]);

	return <></>;
};

const redirect: IRedirect = {
	href: '/not-found',
	statusCode: HTTPStatusCodes.MOVED_PERMANENTLY,
	condition: RedirectConditions.REDIRECT_WHEN_TOKEN_NOT_EXISTS,
};

export const getServerSideProps: GetServerSideProps = async ({
	params,
	res,
}) => {
	if (params && params.token) {
		const checkToken = await checkActivationToken(params.token);
		if (checkToken) removeJwtCookie(res);
		else serverRedirect(res, redirect);
	} else serverRedirect(res, redirect);

	return { props: {} };
};

export default activateToken;
