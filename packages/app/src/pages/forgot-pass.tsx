import { Dispatch, FC, FormEvent, useRef, useState } from 'react';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMutation } from '@apollo/client';
import { toast } from 'react-toastify';
import { FormValidation, GraphlStudent } from 'learnthis-utils';

import Button from '@Components/generic/button/button';
import Input from '@Components/generic/form/input';
import LoginLayout from '@Components/layouts/login.layout';
import { AlertMessages } from '@Config/constants';
import Head from '@Components/generic/head';

import { ButtonTypes } from '@Interfaces/generic/button-types.enum';
import { InputTypes } from '@Interfaces/generic/input-types.enum';
import { ValidationStatus } from '@Interfaces/generic/validation-status.enum';
import { GSSProps } from '@Interfaces/props/gss-props.interface';
import { IRedirect } from '@Interfaces/redirect.interface';

import { RedirectConditions, withAuthGSSP } from '@Lib/utils/ssr.utils';
import { withAuth } from '@Lib/hoc/auth.hoc';

enum ForgotPassFields {
	EMAIL = 'email',
}

interface ForgotPassValidation {
	email: ValidationStatus;
}

const initialState: ForgotPassValidation = {
	email: ValidationStatus.EMPTY,
};

const ForgotPass: FC = () => {
	const [validation, setValidation] = useState<ForgotPassValidation>(
		initialState
	);

	const formRef = useRef<HTMLFormElement>(null);

	const forgotPassMutation = getForgotPassMutation();

	const onSubmitEvent = (event: FormEvent<HTMLFormElement>) =>
		onSubmit(event, formRef.current, setValidation, forgotPassMutation);

	return (
		<>
			<Head
				title='Recuperar contraseña | LearnThis'
				description='Recupera tu contraseña de LearnThis'
				url='/forgot-pass'
			/>
			<LoginLayout title='Recuperar contraseña'>
				<form
					noValidate
					ref={formRef}
					onSubmit={(event: FormEvent<HTMLFormElement>) =>
						onSubmitEvent(event)
					}>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={validation.email === ValidationStatus.ERROR}
						className='mt-0_5'
						label='Email'
						name={ForgotPassFields.EMAIL}
						type={InputTypes.EMAIL}
					/>
					<p className='mt-1'>
						¿Recuerdas tu contraseña?{' '}
						<Link href='/login'>
							<a className='text-teal-600'>Inicia sesión</a>
						</Link>
					</p>
					<Button
						className='mt-1'
						type='submit'
						label='Recuperar'
						kind={ButtonTypes.PRIMARY}></Button>
				</form>
			</LoginLayout>
		</>
	);
};

const validateForm = (form: HTMLFormElement | null): ForgotPassValidation => {
	if (!form) return initialState;

	const formData = {
		email: form[ForgotPassFields.EMAIL].value,
	};

	return {
		email: formData.email
			? Number(FormValidation.emailValidation(formData.email))
			: ValidationStatus.EMPTY,
	};
};

const getForgotPassMutation = () => {
	const router = useRouter();

	const mutation = GraphlStudent.student_create_forgot_password_token;

	const [forgotPassMutation] = useMutation(mutation, {
		onCompleted: () => {
			toast.success(AlertMessages.FORGOT_PASS_SENT);
			router.push('/');
		},
		onError: () => {
			// TODO: ERROR
		},
	});

	return forgotPassMutation;
};

const onBlur = (
	form: HTMLFormElement | null,
	setValidation: Dispatch<ForgotPassValidation>
) => {
	const newValidation = validateForm(form);
	setValidation(newValidation);
};

const onSubmit = async (
	event: FormEvent<HTMLFormElement>,
	form: HTMLFormElement | null,
	setValidation: Dispatch<ForgotPassValidation>,
	forgotPassMutation: Function
): Promise<void> => {
	event.preventDefault();
	if (!form) return;

	const validateSubmit = validateForm(form);
	const validationValues = Object.values(validateSubmit);

	if (validationValues.reduce((a, b) => a + b) === validationValues.length) {
		forgotPassMutation({
			variables: { email: form[ForgotPassFields.EMAIL].value },
		});
	} else {
		if (validateSubmit.email === ValidationStatus.EMPTY)
			validateSubmit.email = ValidationStatus.ERROR;
		setValidation(validateSubmit);
	}
};

const redirect: IRedirect = {
	href: '/',
	statusCode: 302,
	condition: RedirectConditions.REDIRECT_WHEN_USER_EXISTS,
};

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = {};

	const authProps = await withAuthGSSP(ctx, redirect);
	if (authProps) props.authProps = authProps;

	return { props };
};

export default withAuth(ForgotPass, redirect);
