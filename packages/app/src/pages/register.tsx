import { Dispatch, FC, FormEvent, useRef, useState } from 'react';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMutation } from '@apollo/react-hooks';
import { FormValidation, GraphlStudent } from 'learnthis-utils';

import Button from '@Components/generic/button/button';
import Input from '@Components/generic/form/input';
import LoginLayout from '@Components/layouts/login.layout';
import Head from '@Components/generic/head';

import { ButtonTypes } from '@Interfaces/generic/button-types.enum';
import { InputTypes } from '@Interfaces/generic/input-types.enum';
import { ValidationStatus } from '@Interfaces/generic/validation-status.enum';
import { IRedirect } from '@Interfaces/redirect.interface';
import { GSSProps } from '@Interfaces/props/gss-props.interface';

import { RedirectConditions, withAuthGSSP } from '@Lib/utils/ssr.utils';
import { withAuth } from '@Lib/hoc/auth.hoc';
import { toast } from 'react-toastify';
import { AlertMessages } from '@Config/constants';

enum RegisterFields {
	EMAIL = 'user_email',
	PASSWORD = 'user_password',
	CONFIRM_PASSWORD = 'user_conf_pass',
	NAME = 'user_name',
	SURNAME = 'user_surname',
}

interface RegisterValidation {
	email: ValidationStatus;
	password: ValidationStatus;
	confirmPassword: ValidationStatus;
	name: ValidationStatus;
	surname: ValidationStatus;
	didPasswordsMatch: ValidationStatus;
}

const initialState = {
	email: ValidationStatus.EMPTY,
	password: ValidationStatus.EMPTY,
	confirmPassword: ValidationStatus.EMPTY,
	name: ValidationStatus.EMPTY,
	surname: ValidationStatus.EMPTY,
	didPasswordsMatch: ValidationStatus.EMPTY,
};

const Register: FC = () => {
	const [validation, setValidation] = useState<RegisterValidation>(
		initialState
	);

	const formRef = useRef<HTMLFormElement>(null);

	const registerMutation = getRegisterMutation();

	const onSubmitEvent = (event: FormEvent<HTMLFormElement>) =>
		onSubmit(event, formRef.current, setValidation, registerMutation);

	return (
		<>
			<Head
				title='Registro | LearnThis'
				description='Regístrate en LearnThis'
				url='/register'
			/>
			<LoginLayout title='Registro'>
				<form
					noValidate
					ref={formRef}
					onSubmit={(event: FormEvent<HTMLFormElement>) =>
						onSubmitEvent(event)
					}>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={validation.name === ValidationStatus.ERROR}
						className='mt-0_5'
						label='Nombre'
						name={RegisterFields.NAME}
						type={InputTypes.TEXT}
					/>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={validation.surname === ValidationStatus.ERROR}
						className='mt-0_5'
						label='Apellidos'
						name={RegisterFields.SURNAME}
						type={InputTypes.TEXT}
					/>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={validation.email === ValidationStatus.ERROR}
						className='mt-0_5'
						label='Email'
						name={RegisterFields.EMAIL}
						type={InputTypes.EMAIL}
					/>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={
							validation.password === ValidationStatus.ERROR ||
							validation.didPasswordsMatch === ValidationStatus.ERROR
						}
						errorMsg={
							validation.didPasswordsMatch === ValidationStatus.ERROR
								? 'Las contraseñas no coinciden'
								: undefined
						}
						className='mt-0_5'
						label='Contraseña'
						name={RegisterFields.PASSWORD}
						type={InputTypes.PASSWORD}
					/>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation, true)}
						error={
							validation.confirmPassword === ValidationStatus.ERROR ||
							validation.didPasswordsMatch === ValidationStatus.ERROR
						}
						errorMsg={
							validation.didPasswordsMatch === ValidationStatus.ERROR
								? 'Las contraseñas no coinciden'
								: undefined
						}
						className='mt-0_5'
						label='Confirmar contraseña'
						name={RegisterFields.CONFIRM_PASSWORD}
						type={InputTypes.PASSWORD}
					/>
					<p className='mt-1'>
						¿Ya tienes cuenta?{' '}
						<Link href='/login'>
							<a className='text-teal-600'>Iniciar sesión</a>
						</Link>
					</p>
					<Button
						className='mt-1'
						type='submit'
						label='Regístrate'
						kind={ButtonTypes.PRIMARY}></Button>
				</form>
			</LoginLayout>
		</>
	);
};

const getRegisterMutation = () => {
	const router = useRouter();
	const mutation = GraphlStudent.student_register;

	const [registerMutation] = useMutation(mutation, {
		onCompleted: () => {
			toast.success(AlertMessages.REGISTER_SUCCESS);
			router.push('/');
		},
		onError: error => {
			console.dir(error);
		},
	});

	return registerMutation;
};

const validateForm = (
	form: HTMLFormElement | null,
	comparePasswords?: boolean
): RegisterValidation => {
	if (!form) return initialState;

	const formData = {
		email: form[RegisterFields.EMAIL].value,
		password: form[RegisterFields.PASSWORD].value,
		confirmPassword: form[RegisterFields.CONFIRM_PASSWORD].value,
		name: form[RegisterFields.NAME].value,
		surname: form[RegisterFields.SURNAME].value,
	};

	return {
		email: formData.email
			? Number(FormValidation.emailValidation(formData.email))
			: ValidationStatus.EMPTY,
		password: formData.password
			? Number(FormValidation.passwordValidation(formData.password))
			: ValidationStatus.EMPTY,
		confirmPassword: formData.confirmPassword
			? Number(FormValidation.passwordValidation(formData.confirmPassword))
			: ValidationStatus.EMPTY,
		name: formData.name
			? Number(FormValidation.nameValidation(formData.name))
			: ValidationStatus.EMPTY,
		surname: formData.surname
			? Number(FormValidation.nameValidation(formData.surname))
			: ValidationStatus.EMPTY,
		didPasswordsMatch: comparePasswords
			? formData.password === formData.confirmPassword
				? ValidationStatus.SUCCESS
				: ValidationStatus.ERROR
			: ValidationStatus.EMPTY,
	};
};

const onBlur = (
	form: HTMLFormElement | null,
	setValidation: Dispatch<RegisterValidation>,
	comparePasswords?: boolean
) => {
	const newValidation = validateForm(form, comparePasswords);
	setValidation(newValidation);
};

const onSubmit = (
	event: FormEvent<HTMLFormElement>,
	form: HTMLFormElement | null,
	setValidation: Dispatch<RegisterValidation>,
	registerMutation: any
) => {
	event.preventDefault();
	if (!form) return;

	const validateSubmit = validateForm(form, true);
	const validationValues = Object.values(validateSubmit);

	if (validationValues.reduce((a, b) => a + b) === validationValues.length) {
		registerMutation({
			variables: {
				input: {
					email: form[RegisterFields.EMAIL].value,
					password: form[RegisterFields.PASSWORD].value,
					name: form[RegisterFields.NAME].value,
					surname: form[RegisterFields.SURNAME].value,
				},
			},
		});
	} else setValidation(validateSubmit);
};

const redirect: IRedirect = {
	href: '/',
	statusCode: 302,
	condition: RedirectConditions.REDIRECT_WHEN_USER_EXISTS,
};

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = {};

	const authProps = await withAuthGSSP(ctx, redirect);
	if (authProps) props.authProps = authProps;

	return { props };
};

export default withAuth(Register, redirect);
