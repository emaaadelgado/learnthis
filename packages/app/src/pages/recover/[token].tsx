import { Dispatch, FC, FormEvent, useRef, useState } from 'react';
import { GetServerSideProps } from 'next';

import { IRedirect } from '@Interfaces/redirect.interface';
import { HTTPStatusCodes } from '@Interfaces/http-status-codes.enum';
import { RedirectConditions, serverRedirect } from '@Lib/utils/ssr.utils';
import { checkRecoverToken, removeJwtCookie } from '@Lib/utils/user.utils';
import Head from '@Components/generic/head';
import LoginLayout from '@Components/layouts/login.layout';
import { ButtonTypes } from '@Interfaces/generic/button-types.enum';
import Input from '@Components/generic/form/input';
import { ValidationStatus } from '@Interfaces/generic/validation-status.enum';
import Button from '@Components/generic/button/button';
import { InputTypes } from '@Interfaces/generic/input-types.enum';
import { FormValidation, GraphlStudent } from 'learnthis-utils';
import { useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import { AlertMessages } from '@Config/constants';
import { GSSProps } from '@Interfaces/props/gss-props.interface';

enum ChangePasswordFields {
	PASSWORD = 'user_password',
	CONFIRM_PASSWORD = 'user_conf_pass',
}

interface ChangePasswordValidation {
	password: ValidationStatus;
	confirmPassword: ValidationStatus;
	didPasswordsMatch: ValidationStatus;
}

const initialState = {
	password: ValidationStatus.EMPTY,
	confirmPassword: ValidationStatus.EMPTY,
	didPasswordsMatch: ValidationStatus.EMPTY,
};

const recoverToken: FC<any> = ({ token }) => {
	if (!token) return <></>;

	const [validation, setValidation] = useState<ChangePasswordValidation>(
		initialState
	);

	const formRef = useRef<HTMLFormElement>(null);

	const changePasswordMutation = getChangePasswordMutation();

	const onSubmitEvent = (event: FormEvent<HTMLFormElement>) =>
		onSubmit(
			event,
			token,
			formRef.current,
			setValidation,
			changePasswordMutation
		);

	return (
		<>
			<Head
				title='Cambiar contraseña | LearnThis'
				description='Cambia tu contraseña de LearnThis'
				url={`/recover/${token}`}
			/>
			<LoginLayout title='Cambiar contraseña'>
				<form
					noValidate
					ref={formRef}
					onSubmit={(event: FormEvent<HTMLFormElement>) =>
						onSubmitEvent(event)
					}>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={
							validation.password === ValidationStatus.ERROR ||
							validation.didPasswordsMatch === ValidationStatus.ERROR
						}
						errorMsg={
							validation.didPasswordsMatch === ValidationStatus.ERROR
								? 'Las contraseñas no coinciden'
								: undefined
						}
						className='mt-0_5'
						label='Contraseña'
						name={ChangePasswordFields.PASSWORD}
						type={InputTypes.PASSWORD}
					/>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation, true)}
						error={
							validation.confirmPassword === ValidationStatus.ERROR ||
							validation.didPasswordsMatch === ValidationStatus.ERROR
						}
						errorMsg={
							validation.didPasswordsMatch === ValidationStatus.ERROR
								? 'Las contraseñas no coinciden'
								: undefined
						}
						className='mt-0_5'
						label='Confirmar contraseña'
						name={ChangePasswordFields.CONFIRM_PASSWORD}
						type={InputTypes.PASSWORD}
					/>
					<Button
						className='mt-1'
						type='submit'
						label='Cambiar contraseña'
						kind={ButtonTypes.PRIMARY}></Button>
				</form>
			</LoginLayout>
		</>
	);
};

const getChangePasswordMutation = () => {
	const router = useRouter();
	const mutation = GraphlStudent.student_change_forgot_password;

	const [changePasswordMutation] = useMutation(mutation, {
		onCompleted: () => {
			toast.success(AlertMessages.PASSWORD_CHANGED);
			router.push('/login');
		},
		onError: () => {
			// TODO ERROR
		},
	});

	return changePasswordMutation;
};

const validateForm = (
	form: HTMLFormElement | null,
	comparePasswords?: boolean
): ChangePasswordValidation => {
	if (!form) return initialState;

	const formData = {
		password: form[ChangePasswordFields.PASSWORD].value,
		confirmPassword: form[ChangePasswordFields.CONFIRM_PASSWORD].value,
	};

	return {
		password: formData.password
			? Number(FormValidation.passwordValidation(formData.password))
			: ValidationStatus.EMPTY,
		confirmPassword: formData.confirmPassword
			? Number(FormValidation.passwordValidation(formData.confirmPassword))
			: ValidationStatus.EMPTY,
		didPasswordsMatch: comparePasswords
			? formData.password === formData.confirmPassword
				? ValidationStatus.SUCCESS
				: ValidationStatus.ERROR
			: ValidationStatus.EMPTY,
	};
};

const onBlur = (
	form: HTMLFormElement | null,
	setValidation: Dispatch<ChangePasswordValidation>,
	comparePasswords?: boolean
) => {
	const newValidation = validateForm(form, comparePasswords);
	setValidation(newValidation);
};

const onSubmit = async (
	event: FormEvent<HTMLFormElement>,
	token: string,
	form: HTMLFormElement | null,
	setValidation: Dispatch<ChangePasswordValidation>,
	changePasswordMutation: Function
): Promise<void> => {
	event.preventDefault();
	if (!form) return;

	const validateSubmit = validateForm(form, true);
	const validationValues = Object.values(validateSubmit);
	console.log(token, form[ChangePasswordFields.PASSWORD].value);
	if (validationValues.reduce((a, b) => a + b) === validationValues.length) {
		changePasswordMutation({
			variables: {
				input: {
					token,
					newPassword: form[ChangePasswordFields.PASSWORD].value,
				},
			},
		});
	} else setValidation(validateSubmit);
};

const redirect: IRedirect = {
	href: '/not-found',
	statusCode: HTTPStatusCodes.MOVED_PERMANENTLY,
	condition: RedirectConditions.REDIRECT_WHEN_TOKEN_NOT_EXISTS,
};

export const getServerSideProps: GetServerSideProps = async ({
	params,
	res,
}) => {
	if (params && params.token) {
		const checkToken = await checkRecoverToken(params.token);
		if (checkToken) removeJwtCookie(res);
		else serverRedirect(res, redirect);
	} else serverRedirect(res, redirect);

	const props: GSSProps = {};
	props.componentProps = { token: params?.token };

	return { props };
};

export default recoverToken;
