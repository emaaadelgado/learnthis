import { Dispatch, FC, FormEvent, useContext, useRef, useState } from 'react';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { NextRouter, useRouter } from 'next/router';
import Link from 'next/link';
import { toast } from 'react-toastify';
import { FormValidation } from 'learnthis-utils';

import Button from '@Components/generic/button/button';
import Input from '@Components/generic/form/input';
import LoginLayout from '@Components/layouts/login.layout';
import Head from '@Components/generic/head';

import { ButtonTypes } from '@Interfaces/generic/button-types.enum';
import { InputTypes } from '@Interfaces/generic/input-types.enum';
import { ValidationStatus } from '@Interfaces/generic/validation-status.enum';
import { RestEndPoints } from '@Interfaces/rest.enum';
import { IRedirect } from '@Interfaces/redirect.interface';
import { GSSProps } from '@Interfaces/props/gss-props.interface';
import { IAuthContext } from '@Interfaces/auth-context.interface';

import { fetchWithTimeout } from '@Lib/utils/fetch.utils';
import { AuthContext } from '@Lib/context/auth.context';
import { RedirectConditions, withAuthGSSP } from '@Lib/utils/ssr.utils';
import { withAuth } from '@Lib/hoc/auth.hoc';

import { AlertMessages } from '@Config/constants';

enum LoginFields {
	EMAIL = 'email',
	PASSWORD = 'password',
}

interface LoginValidation {
	email: ValidationStatus;
	password: ValidationStatus;
}

const initialState = {
	email: ValidationStatus.EMPTY,
	password: ValidationStatus.EMPTY,
};

const Login: FC = () => {
	const [validation, setValidation] = useState<LoginValidation>(initialState);

	const { setAuth } = useContext(AuthContext);
	const formRef = useRef<HTMLFormElement>(null);
	const router = useRouter();

	const onSubmitEvent = (event: FormEvent<HTMLFormElement>) =>
		onSubmit(event, formRef.current, setValidation, setAuth, router);

	return (
		<>
			<Head
				title='Iniciar sesión | LearnThis'
				description='Inicia sesión en LearnThis'
				url='/login'
			/>
			<LoginLayout title='Iniciar sesión'>
				<form
					noValidate
					ref={formRef}
					onSubmit={(event: FormEvent<HTMLFormElement>) =>
						onSubmitEvent(event)
					}>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={validation.email === ValidationStatus.ERROR}
						className='mt-0_5'
						label='Email'
						name={LoginFields.EMAIL}
						type={InputTypes.EMAIL}
					/>
					<Input
						onBlur={() => onBlur(formRef.current, setValidation)}
						error={validation.password === ValidationStatus.ERROR}
						errorMsg='Introduce una contraseña'
						className='mt-0_5'
						label='Contraseña'
						name={LoginFields.PASSWORD}
						type={InputTypes.PASSWORD}
					/>
					<p className='mt-1'>
						<Link href='/forgot-pass'>
							<a className='text-teal-600'>Olvidé mi contraseña</a>
						</Link>
					</p>
					<Button
						className='mt-1'
						type='submit'
						label='Iniciar sesión'
						kind={ButtonTypes.PRIMARY}></Button>
					<p className='mt-1'>
						¿Todavía no tienes cuenta?{' '}
						<Link href='/register'>
							<a className='text-teal-600'>Regístrate</a>
						</Link>
					</p>
				</form>
			</LoginLayout>
		</>
	);
};

const validateForm = (form: HTMLFormElement | null): LoginValidation => {
	if (!form) return initialState;

	const formData = {
		email: form[LoginFields.EMAIL].value,
		password: form[LoginFields.PASSWORD].value,
	};

	return {
		email: formData.email
			? Number(FormValidation.emailValidation(formData.email))
			: ValidationStatus.EMPTY,
		password: formData.password.length
			? ValidationStatus.SUCCESS
			: ValidationStatus.EMPTY,
	};
};

const onBlur = (
	form: HTMLFormElement | null,
	setValidation: Dispatch<LoginValidation>
) => {
	const newValidation = validateForm(form);
	setValidation(newValidation);
};

const onSubmit = async (
	event: FormEvent<HTMLFormElement>,
	form: HTMLFormElement | null,
	setValidation: Dispatch<LoginValidation>,
	setAuth: Dispatch<IAuthContext>,
	router: NextRouter
) => {
	event.preventDefault();
	if (!form) return;

	const validateSubmit = validateForm(form);
	const validationValues = Object.values(validateSubmit);

	if (validationValues.reduce((a, b) => a + b) === validationValues.length) {
		const req = {
			email: form[LoginFields.EMAIL].value,
			password: form[LoginFields.PASSWORD].value,
		};

		form[LoginFields.PASSWORD].value = '';

		try {
			const response = await fetchWithTimeout(
				RestEndPoints.LOGIN,
				req,
				Number(process.env.NEXT_PUBLIC_REQ_TIMEOUT)
			);

			const responseJson = await response.json();

			if (responseJson.error) {
				toast.error(responseJson.error, { toastId: 1 });
				setValidation(validateSubmit);
			} else {
				const jwt = responseJson.token;
				const student = responseJson.user;

				setAuth({ jwt, student });

				toast.success(AlertMessages.WELCOME);
				router.push('/');
			}
		} catch (error) {
			console.log(error);
			toast.error(AlertMessages.SERVER_ERROR, { toastId: 1 });
		}
	} else {
		if (validateSubmit.password === ValidationStatus.EMPTY)
			validateSubmit.password = ValidationStatus.ERROR;
		setValidation(validateSubmit);
	}
};

const redirect: IRedirect = {
	href: '/',
	statusCode: 302,
	condition: RedirectConditions.REDIRECT_WHEN_USER_EXISTS,
};

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = {};

	const authProps = await withAuthGSSP(ctx, redirect);
	if (authProps) props.authProps = authProps;

	return { props };
};

export default withAuth(Login, redirect);
