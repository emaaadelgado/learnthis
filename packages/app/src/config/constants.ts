export const JWT_COOKIE_NAME = 'jid';
export enum AlertMessages {
	WELCOME = 'Bienvenido a LearnThis',
	SERVER_ERROR = 'Se ha producido un error en el servidor',
	FORGOT_PASS_SENT = 'Se han enviado las instrucciones de recuperación a su correo. Por favor, revíselo.',
	ACTIVATION_SUCCESS = 'Ya puede iniciar sesión con su cuenta',
	PASSWORD_CHANGED = 'Contraseña cambiada con éxito',
	REGISTER_SUCCESS = 'Se le ha enviado un correo de activación, active su cuenta para poder iniciar sesión',
}
