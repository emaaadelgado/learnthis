import {
	usernameValidation,
	birthDateValidation,
	emailValidation,
	genderValidation,
	nameValidation,
	passwordValidation,
	bioValidation,
} from './validation/formValidation';

import {
	student_login,
	student_valid_forgot_password_token,
	student_register,
	student_profile,
	student_profile_min,
	student_activate_account,
	student_create_forgot_password_token,
	student_change_forgot_password,
} from './graphql/student.gql';

export { tokenValidation } from './validation/tokenValidation';

export const FormValidation = {
	usernameValidation,
	birthDateValidation,
	emailValidation,
	genderValidation,
	nameValidation,
	passwordValidation,
	bioValidation,
};

export const GraphlStudent = {
	student_login,
	student_valid_forgot_password_token,
	student_register,
	student_profile,
	student_profile_min,
	student_activate_account,
	student_create_forgot_password_token,
	student_change_forgot_password,
};
