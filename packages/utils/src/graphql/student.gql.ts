import gql from 'graphql-tag';

export const student_login = gql`
	query student_login($input: LoginDto!) {
		student_login(input: $input)
	}
`;

export const student_valid_forgot_password_token = gql`
	query student_valid_forgot_password_token($token: String!) {
		student_valid_forgot_password_token(token: $token)
	}
`;

export const student_profile = gql`
	query student_profile {
		student_profile {
			email
			name
			surname
			username
			photo
			bio
			gender
			birthDate
		}
	}
`;

export const student_profile_min = gql`
	query student_profile_min {
		student_profile {
			email
			name
			surname
			username
			photo
		}
	}
`;

export const student_register = gql`
	mutation student_register($input: RegisterDto!) {
		student_register(input: $input)
	}
`;

export const student_activate_account = gql`
	mutation student_activate_account($token: String!) {
		student_activate_account(token: $token)
	}
`;

export const student_create_forgot_password_token = gql`
	mutation student_create_forgot_password_token($email: String!) {
		student_create_forgot_password_token(email: $email)
	}
`;

export const student_change_forgot_password = gql`
	mutation student_change_forgot_password($input: RecoverPasswordDto!) {
		student_change_forgot_password(input: $input)
	}
`;
