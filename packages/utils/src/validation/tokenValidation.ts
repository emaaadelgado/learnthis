import { decode } from 'jsonwebtoken';

export const tokenValidation = (token: string): boolean => {
	if (decode(token)) return true;
	return false;
};
