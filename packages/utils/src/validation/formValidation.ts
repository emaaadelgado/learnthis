/**
 * Validación del username
 * Only contains alphanumeric characters, underscore and dot.
 * Underscore and dot can't be at the end or start of a username (e.g _username / username_ / .username / username.).
 * Underscore and dot can't be next to each other (e.g user_.name).
 * Underscore or dot can't be used multiple times in a row (e.g user__name / user..name).
 * Number of characters must be between 3 to 20.
 * @param value
 */
export const usernameValidation = (value: string) => {
	const userName = value.trim();
	const valExp = /^(?=[a-zA-Z0-9._]{3,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/;

	if (!userName.match(valExp)) return false;

	return true;
};

/**
 * Validación del nombre.
 * @param {String} value Valor a validar
 */
export const nameValidation = (value: string) => {
	const name = value.trim();
	const valExp = /^([a-zA-ZÀ-ÿ][-,a-z. ']+[ ]*)+$/;

	if (name.length < 1) return false;
	if (!name.match(valExp)) return false;

	return true;
};

/**
 * Validación del email.
 * @param {String} value Valor a validar
 */
export const emailValidation = (value: string) => {
	const email = value.trim();
	const valExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if (email.length < 6) return false;
	if (!email.match(valExp)) return false;

	return true;
};

/**
 * Validación de la contraseña
 * @param {String} value Valor a validar
 */
export const passwordValidation = (value: string) => {
	const pass = value.trim();
	const valExp = /^[\x21-\x7E]+$/;

	if (pass.length < 6) return false;
	if (!pass.match(valExp)) return false;

	return true;
};

/**
 * Validación del género
 * @param {String} value  Valor a validar
 */
export const genderValidation = (value: string) => {
	const gender = value.trim();
	const valExp = /^(Male|Female|Other gender|Rather not say)$/g;

	if (!gender.match(valExp)) return false;

	return true;
};

/**
 * Validación de la fecha de nacimiento
 * @param {String} value  Valor a validar
 */
export const birthDateValidation = (date: Date) => {
	if (isNaN(date.getTime())) return false;

	let dateLimit = new Date();
	dateLimit.setFullYear(dateLimit.getFullYear() - 13);

	if (date > dateLimit) return false;

	return true;
};

export const bioValidation = (bio: string) => {
	if (bio.length < 10) return false;

	return true;
};
